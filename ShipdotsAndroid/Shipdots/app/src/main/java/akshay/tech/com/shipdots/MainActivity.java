package akshay.tech.com.shipdots;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import akshay.tech.com.shipdots.OtherTools.Chat;
import akshay.tech.com.shipdots.OtherTools.CreateOrder;
import akshay.tech.com.shipdots.OtherTools.InternetConnection;
import akshay.tech.com.shipdots.OtherTools.Orders;
import akshay.tech.com.shipdots.OtherTools.Profile;
import akshay.tech.com.shipdots.OtherTools.ViewImage;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static akshay.tech.com.shipdots.SplashScreen.conn;
import static akshay.tech.com.shipdots.SplashScreen.db_password;
import static akshay.tech.com.shipdots.SplashScreen.db_username;
import static akshay.tech.com.shipdots.SplashScreen.stmt;
import static akshay.tech.com.shipdots.SplashScreen.url;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static String final_id = "", final_password = "";
    File password_file = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/pw.txt");
    private Uri mCropImageUri;
    public static CircleImageView userImage;
    RelativeLayout new_order, shipments, account;
    ArrayList<String> addresses;
    HashMap<String, String> customerDetails;
    public static int MONEY_TO_ADD;
    public static String ImageLink, Name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String ID = getIntent().getStringExtra("ID");
        final String PASSWORD = getIntent().getStringExtra("PASSWORD");
        addresses = getIntent().getStringArrayListExtra("ADDRESS_LIST");

        if(!password_file.exists()) {
            try {
                password_file.createNewFile();
                FileWriter fileWriter = new FileWriter(password_file, false);
                fileWriter.write(ID);
                fileWriter.write(" ");
                fileWriter.write(PASSWORD);
                fileWriter.flush();
                fileWriter.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        final_id = ID;
        final_password = PASSWORD;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dashboard");

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        if(InternetConnection.checkConnection(getApplicationContext())) {
            try {
                customerDetails = new getCustomerDetails().execute().get();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.myDialog)).setTitle("Connection Error")
                    .setMessage("Failed to establish a network connection. Please enable Wi-Fi or Data Connection to continue.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finishAffinity();
                        }
                    })
                    .show();
        }

        Name = customerDetails.get("NAME");
        ImageLink = customerDetails.get("IMAGE_LINK");
        final String MemberPlan = customerDetails.get("MEMBER_PLAN");

        if(MemberPlan!=null) {
            switch (MemberPlan) {
                case "DIAMOND":
                    MONEY_TO_ADD = 2;
                    break;
                case "GOLD":
                    MONEY_TO_ADD = 4;
                    break;
                case "SILVER":
                    MONEY_TO_ADD = 8;
                    break;
                default:
                    MONEY_TO_ADD = 20;
                    break;
            }
        }

        MONEY_TO_ADD = 20;

        View nav_view = navigationView.getHeaderView(0);

        userImage = nav_view.findViewById(R.id.circleImageView);

        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable(){
            @Override
            public void run() {
                Picasso.with(getBaseContext()).load(ImageLink).into(userImage);
            }
        });

        TextView name = nav_view.findViewById(R.id.the_name);
        name.setText(Name);

        TextView user_id = nav_view.findViewById(R.id.the_user_id);
        user_id.setText(ID);

        new_order = findViewById(R.id.new_order_tab);
        new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    vibrate();
                startActivity(new Intent(MainActivity.this, CreateOrder.class).putExtra("ADDRESS_LIST", addresses));
            }
        });

        shipments = findViewById(R.id.view_shipments_tab);
        shipments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                startActivity(new Intent(MainActivity.this, Orders.class));
            }
        });

        account = findViewById(R.id.account_settings_tab);
        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                startActivity(new Intent(MainActivity.this, Profile.class).putExtra("IMAGE_LINK", ImageLink)
                .putExtra("NAME", Name));
            }
        });

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.myDialog)).setTitle("Select an option").setMessage("What do you want to do?")
                        .setPositiveButton("View Image", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent showImage = new Intent(MainActivity.this, ViewImage.class);
                    showImage.putExtra("URL", ImageLink);
                    startActivity(showImage);
                }
            })
                    .setNegativeButton("Change Image", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    changeImage();
                }
            })
                    .show();
        }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog)).setTitle("Exit").setMessage("Do you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_chat) {
            startActivity(new Intent(MainActivity.this, Chat.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_orders) {
            Intent orders = new Intent(MainActivity.this, Orders.class);
            startActivity(orders);
        } else if (id == R.id.nav_new_order) {
            startActivity(new Intent(MainActivity.this, CreateOrder.class).putExtra("ADDRESS_LIST", addresses));
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(MainActivity.this, Profile.class).putExtra("IMAGE_LINK", ImageLink)
                    .putExtra("NAME", Name));
        }else if (id == R.id.nav_logout) {
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog)).setTitle("Logout").setMessage("You'll be logged out. Are you sure?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if(password_file.exists()) {
                                if(password_file.delete()) {
                                    Toast.makeText(MainActivity.this, "Logging Out", Toast.LENGTH_SHORT).show();
                                    Intent logout = new Intent(MainActivity.this, LoginScreen.class);
                                    startActivity(logout);
                                }
                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    })
                    .show();
        } else if (id == R.id.nav_chat) {
            Intent chat = new Intent(MainActivity.this, Chat.class);
            startActivity(chat);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeImage() {
        startActivityForResult(CropImage.getPickImageChooserIntent(getBaseContext()), CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getBaseContext(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getBaseContext(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                ((CircleImageView) findViewById(R.id.circleImageView)).setImageURI(result.getUri());
                    Bitmap photo = ((BitmapDrawable)userImage.getDrawable()).getBitmap();
                    new uploadImage().execute(photo);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getBaseContext(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(getBaseContext(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }


    @SuppressLint("StaticFieldLeak")
    public class getCustomerDetails extends AsyncTask<Void, Void, HashMap> {

        private ProgressDialog progressDialog;
        HashMap<String, String> user_details = new HashMap<>();

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Syncing..");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected HashMap<String, String> doInBackground(Void... voids) {

           StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
           StrictMode.setThreadPolicy(policy);
            try {
              //Class.forName("com.mysql.jdbc.Driver");
               //Connection conn = DriverManager.getConnection(SplashScreen.url, SplashScreen.db_username, SplashScreen.db_password);
               // Statement st = conn.createStatement();
                //ResultSet rs = stmt.executeQuery("SELECT * FROM customer_details WHERE Email = '"+final_id+"' AND Password ='"+final_password+"'");
                if(conn!=null){
                    if(conn.isClosed()) {
                        conn = DriverManager.getConnection(url, db_username, db_password);
                        //stmt = conn.createStatement();
                    }
                ResultSet rs = stmt.executeQuery("SELECT FirstName,LastName,ImageLink,Wallet,Member_Plan FROM customer_details WHERE Email = '"+final_id+"' AND Password ='"+final_password+"'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {

                        user_details.put("NAME", rs.getString("FirstName")+" "+rs.getString("LastName"));
                        user_details.put("IMAGE_LINK", rs.getString("ImageLink"));
                        user_details.put("WALLET", rs.getString("Wallet"));
                        user_details.put("MEMBER_PLAN", rs.getString("Member_Plan"));
                    }
                }
                else {
                    if(password_file.exists()) {
                        if(password_file.delete()) {
                            Toast.makeText(MainActivity.this, "Login ID or Password Changed. Login Again!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, LoginScreen.class));
                            finish();
                        }
                    }
                }
              // conn.close();

            }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return user_details;
        }

        @Override
        protected void onPostExecute(HashMap hashMap) {
            progressDialog.dismiss();
            customerDetails = hashMap;
            super.onPostExecute(hashMap);
        }
    }


    @SuppressLint("StaticFieldLeak")
    public class uploadImage extends AsyncTask<Bitmap, Void, Void> {

        private ProgressDialog p;
        String message = "";

        @Override
        protected void onPreExecute() {
            p = new ProgressDialog(MainActivity.this);
            p.setMessage("Updating Image..");
            p.setIndeterminate(false);
            p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p.setCancelable(false);
            p.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Bitmap... bitmaps) {

            Bitmap image = bitmaps[0];

            String imageName = final_id+".jpg";
            OutputStream os = null;
            try {
                os = new BufferedOutputStream(new FileOutputStream(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/"+imageName));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            image.compress(Bitmap.CompressFormat.JPEG, 60, os);

            File file = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/"+imageName);

            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("submit_image", file.getName(), RequestBody.create(MediaType.parse("image/jpeg"), file)).build();
            Request request = new Request.Builder().url("https://app.vourier.com/upload_image.php").post(requestBody).build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if(response.body() != null) {
                    String uploadResponse = response.body().string();
                    Log.e("RESPONSE", uploadResponse);
                    if (uploadResponse.equals("done")) {
                        message = "Profile picture changed";
                        deleteCache(MainActivity.this);
                    }
                    else {
                        message = "Upload failed";
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            p.dismiss();
            final DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            Snackbar.make(drawerLayout, message, Snackbar.LENGTH_LONG).show();
            super.onPostExecute(aVoid);
        }

        void deleteCache(Context context) {
            try {
                File dir = context.getCacheDir();
                deleteDir(dir);
            } catch (Exception e) { e.printStackTrace();}
        }

        boolean deleteDir(File dir) {
            if (dir != null && dir.isDirectory()) {
                String[] children = dir.list();
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
                return dir.delete();
            } else if(dir!= null && dir.isFile()) {
                return dir.delete();
            } else {
                return false;
            }
        }
    }

    public void vibrate() {
        Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(100);
        }
    }
}
