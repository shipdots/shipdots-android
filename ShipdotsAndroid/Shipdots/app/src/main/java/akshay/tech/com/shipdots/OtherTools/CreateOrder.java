package akshay.tech.com.shipdots.OtherTools;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.renderscript.Float4;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import akshay.tech.com.shipdots.ConfirmOrder;
import akshay.tech.com.shipdots.MainActivity;
import akshay.tech.com.shipdots.R;
import akshay.tech.com.shipdots.SplashScreen;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static akshay.tech.com.shipdots.MainActivity.final_id;
import static akshay.tech.com.shipdots.SplashScreen.conn;
import static akshay.tech.com.shipdots.SplashScreen.db_password;
import static akshay.tech.com.shipdots.SplashScreen.db_username;
import static akshay.tech.com.shipdots.SplashScreen.stmt;


public class CreateOrder extends AppCompatActivity {

    private MaterialSpinner addressSpinner;
    @SuppressLint("StaticFieldLeak")
    public static EditText pickupFrom, pickupFromComment, fromPincode, fromName, fromNumber, productPrice, addressAlias, cityPincode, pickupCityPincode, parcelWeight;
    private ExpandableRelativeLayout pickupLayout, deliveryLayout, productLayout;
    private TextView deliveryPoint, productPoint;
    private CheckBox saveAddressCheckbox;
    private RadioButton savedAddressRadioButton;
    private EditText toName, toNumber, toAddress, toPincode;
    private EditText parcelLength, parcelBreadth, parcelHeight;
    private CheckBox needInsurance, needPackaging;
    private Button createOrder;
    private RadioGroup packageEnvelope;
    String FROM_NAME, FROM_NUMBER, FROM_ADDRESS, ADDRESS_COMMENT, FROM_PINCODE, ADDRESS_ALIAS, TO_NAME, TO_NUMBER, TO_ADDRESS, TO_PINCODE, LENGTH, BREADTH, HEIGHT, WEIGHT, PRICE = "";
    HashMap<String, String> courier_details;
    public static String Latitude, Longitude = "";
    ArrayList addressAliases;
    CountDownTimer pincodeTimer, toPincodeTimer, pincodeController, fillCalculatedPrice = null;
    double MONEY_TO_ADD = MainActivity.MONEY_TO_ADD;
    double MONEY_TO_SUBTRACT = 0;
    private RadioButton envelopeButton, packageButton;
    android.app.AlertDialog alertDialog;
    private EditText calculatedPrice;
    String HUB_ID = "";
    Geocoder geocoder;
    Double hubLat, hubLang, codeLat, codeLang = 0.0;
    String[] listItem;
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_order);

        addressAliases = getIntent().getStringArrayListExtra("ADDRESS_LIST");

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create New Order");

        addressSpinner = findViewById(R.id.address_spinner);
        addressSpinner.setItems(addressAliases);

        pickupFrom = findViewById(R.id.pickupAddress);
        pickupFrom.setKeyListener(null);

        pickupFromComment = findViewById(R.id.pickupAddressComment);

        fromName = findViewById(R.id.fromName);
        fromNumber = findViewById(R.id.phoneNumber);
        fromPincode = findViewById(R.id.pickupPincode);

        cityPincode = findViewById(R.id.cityPincode);

        pickupCityPincode = findViewById(R.id.pickupCityPincode);

        pickupLayout = findViewById(R.id.pickupLayout);

        productLayout = findViewById(R.id.productDetails);
        productPoint = findViewById(R.id.product);
        productLayout.collapse();

        deliveryLayout = findViewById(R.id.endShipment);
        deliveryPoint = findViewById(R.id.deliveryPoint);
        deliveryLayout.collapse();

        productPrice = findViewById(R.id.parcel_price);
        addressAlias = findViewById(R.id.addressAlias);
        saveAddressCheckbox = findViewById(R.id.saveAddress);

        parcelWeight = findViewById(R.id.parcel_weight);

        envelopeButton = findViewById(R.id.envelop);
        packageButton = findViewById(R.id.box);

        deliveryPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deliveryLayout.toggle();
            }
        });

        productPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productLayout.toggle();
            }
        });

        savedAddressRadioButton = findViewById(R.id.saved_address);

        toName = findViewById(R.id.toName);
        toNumber = findViewById(R.id.phoneNumber_delivery);
        toAddress = findViewById(R.id.deliveryAddress);
        toPincode = findViewById(R.id.deliveryPincode);

        parcelLength = findViewById(R.id.parcel_length);
        parcelBreadth = findViewById(R.id.parcel_breadth);
        parcelHeight = findViewById(R.id.parcel_height);

        needInsurance = findViewById(R.id.insurance);
        needPackaging = findViewById(R.id.packaging);
        packageEnvelope = findViewById(R.id.radioGroup1);
        calculatedPrice = findViewById(R.id.calculated_price);

        createOrder = findViewById(R.id.createOrder);



        //saveAddressCheckbox.setVisibility(View.GONE);
        //addressAlias.setVisibility(View.GONE);



        pickupFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CreateOrder.this, MapActivity.class).putExtra("ADDRESS-TYPE", "PICKUP").putExtra("ACTIVITY_NAME", "CREATE"));
            }
        });

        createOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                submitOrder();
            }
        });

        fromNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                pickupFrom.requestFocus();
                pickupFrom.performClick();
                return true;
            }
        });

        addressAlias.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                deliveryLayout.expand();
                toName.requestFocus();
                return true;
            }
        });

        toPincode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                productLayout.expand();
                parcelWeight.requestFocus();
                return true;
            }
        });

        parcelWeight.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                parcelLength.requestFocus();
                return true;
            }
        });

        parcelLength.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                parcelBreadth.requestFocus();
                return true;
            }
        });

        needPackaging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (needPackaging.isChecked()) {
                    packageEnvelope.setVisibility(View.VISIBLE);
                } else {
                    packageEnvelope.setVisibility(View.GONE);
                }
            }
        });

        addressSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String aliasName = item.toString();
                new getAliasDetails().execute(aliasName);
            }
        });


        pickupCityPincode.setClickable(false);
        pickupCityPincode.setEnabled(false);

        cityPincode.setEnabled(false);
        cityPincode.setClickable(false);

        calculatedPrice.setClickable(false);
        calculatedPrice.setEnabled(false);

        pincodeTimer = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                start();

                if (fromPincode.getText().toString().length() == 6) {
                    new getCity().execute("FROM", fromPincode.getText().toString());
                    pincodeTimer.cancel();
                }

            }
        }.start();

        toPincodeTimer = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                start();
                if (toPincode.getText().toString().length() == 6) {
                    new getCity().execute("TO", toPincode.getText().toString());
                    toPincodeTimer.cancel();
                }
            }
        }.start();

        pincodeController = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                start();

                if (fromPincode.getText().toString().length() != 6)
                    pincodeTimer.start();

                if (toPincode.getText().toString().length() != 6)
                    toPincodeTimer.start();
            }
        }.start();

        fillCalculatedPrice = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFinish() {
                start();

                if (calculatedPrice.getVisibility() == View.VISIBLE) {
                    if (!productPrice.getText().toString().isEmpty()) {
                        double price = Double.parseDouble(productPrice.getText().toString());
                        double final_price = 0.02 * price;

                        calculatedPrice.setText("₹" + final_price);
                    } else {
                        calculatedPrice.setText("");
                    }
                }

            }
        }.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.saved_address:
                if (checked)
                    showSavedAddressTools();
                break;
            case R.id.new_address:
                if (checked)
                showNewAddressTools();
                break;
        }
    }


    public void showSavedAddressTools() {
        addressSpinner.setVisibility(View.VISIBLE);

        addressSpinner.setSelectedIndex(0);

       saveAddressCheckbox.setVisibility(View.GONE);
       addressAlias.setVisibility(View.GONE);

        pickupLayout.setClickable(true);
        pickupLayout.collapse();
    }

    public void showNewAddressTools() {
        addressSpinner.setVisibility(View.GONE);

        saveAddressCheckbox.setVisibility(View.VISIBLE);
        addressAlias.setVisibility(View.VISIBLE);

        pickupLayout.setClickable(false);
        pickupLayout.expand();
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        if (view.getId() == R.id.insurance) {
            if (checked) {
                productPrice.setVisibility(View.VISIBLE);
                calculatedPrice.setVisibility(View.VISIBLE);
            } else {
                productPrice.setVisibility(View.GONE);
                calculatedPrice.setVisibility(View.GONE);
            }
        }
        if (view.getId() == R.id.saveAddress) {
            if (checked)
                addressAlias.setVisibility(View.VISIBLE);
            else
                addressAlias.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog)).setTitle("Exit").setMessage("Are you sure you want to exit? Your filled order details will be lost.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CreateOrder.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .show();
    }

    private void submitOrder() {

        courier_details = new HashMap<>();

        int fromMobile = fromNumber.getText().toString().length();
        int toMobile = toNumber.getText().toString().length();

        if (savedAddressRadioButton.isChecked()) {
            if (!fromName.getText().toString().isEmpty() && !fromNumber.getText().toString().isEmpty() && !pickupFrom.getText().toString().isEmpty() && !pickupFromComment.getText().toString().isEmpty() && !fromPincode.getText().toString().isEmpty() && !pickupCityPincode.getText().toString().isEmpty()) {
                if (!toName.getText().toString().isEmpty() && !toNumber.getText().toString().isEmpty() && !toAddress.getText().toString().isEmpty() && !toPincode.getText().toString().isEmpty() && !cityPincode.getText().toString().isEmpty()) {
                    if (!parcelWeight.getText().toString().isEmpty() && !parcelLength.getText().toString().isEmpty() && !parcelBreadth.getText().toString().isEmpty() && !parcelHeight.getText().toString().isEmpty()) {
                        if (needInsurance.isChecked()) {
                            if (!productPrice.getText().toString().isEmpty() && Integer.parseInt(productPrice.getText().toString()) > 0) {
                                if (toMobile != 10 || fromMobile != 10) {
                                    if (toMobile != 10)
                                        toNumber.setError("Mobile number invalid!");
                                    if (fromMobile != 10)
                                        fromNumber.setError("Mobile number invalid");
                                } else {
                                    FROM_NAME = fromName.getText().toString();
                                    FROM_NUMBER = fromNumber.getText().toString();
                                    FROM_ADDRESS = pickupFrom.getText().toString();
                                    ADDRESS_COMMENT = pickupFromComment.getText().toString();
                                    ADDRESS_ALIAS = "NOT AVAILABLE";
                                    TO_NAME = toName.getText().toString();
                                    TO_NUMBER = toNumber.getText().toString();
                                    TO_ADDRESS = toAddress.getText().toString();
                                    TO_PINCODE = toPincode.getText().toString();
                                    FROM_PINCODE = fromPincode.getText().toString();
                                    LENGTH = parcelLength.getText().toString();
                                    BREADTH = parcelBreadth.getText().toString();
                                    HEIGHT = parcelHeight.getText().toString();
                                    WEIGHT = parcelWeight.getText().toString();
                                    PRICE = productPrice.getText().toString();

                                    new showCouriers().execute();
                                }
                            } else {
                                productPrice.setError("Required and should be greater than 0");
                            }
                        } else {
                            if (toMobile != 10 || fromMobile != 10) {
                                if (toMobile != 10)
                                    toNumber.setError("Mobile number invalid!");
                                if (fromMobile != 10)
                                    fromNumber.setError("Mobile number invalid");
                            } else {
                                FROM_NAME = fromName.getText().toString();
                                FROM_NUMBER = fromNumber.getText().toString();
                                FROM_ADDRESS = pickupFrom.getText().toString();
                                ADDRESS_COMMENT = pickupFromComment.getText().toString();
                                ADDRESS_ALIAS = "NOT AVAILABLE";
                                TO_NAME = toName.getText().toString();
                                TO_NUMBER = toNumber.getText().toString();
                                TO_ADDRESS = toAddress.getText().toString();
                                TO_PINCODE = toPincode.getText().toString();
                                FROM_PINCODE = fromPincode.getText().toString();
                                LENGTH = parcelLength.getText().toString();
                                BREADTH = parcelBreadth.getText().toString();
                                HEIGHT = parcelHeight.getText().toString();
                                WEIGHT = parcelWeight.getText().toString();
                                PRICE = "NOT AVAILABLE";

                                new showCouriers().execute();
                            }
                        }
                    } else {
                        if (!productLayout.isExpanded()) {
                            productLayout.expand();
                        }
                        if (parcelWeight.getText().toString().isEmpty()) {
                            parcelWeight.setError("Required*");
                        }
                        if (parcelLength.getText().toString().isEmpty()) {
                            parcelLength.setError("Required*");
                        }
                        if (parcelBreadth.getText().toString().isEmpty()) {
                            parcelBreadth.setError("Required*");
                        }
                        if (parcelHeight.getText().toString().isEmpty()) {
                            parcelHeight.setError("Required*");
                        }
                        if (parcelWeight.getText().toString().equals("0")) {
                            parcelWeight.setError("Weight cannot be 0 kg!");
                        }
                        if (!parcelLength.getText().toString().isEmpty()) {
                            if (Integer.parseInt(parcelLength.getText().toString()) < 0.5) {
                                parcelLength.setError("Length cannot be less than 10 cms");
                            }
                        }
                        if (!parcelBreadth.getText().toString().isEmpty()) {
                            if (Integer.parseInt(parcelBreadth.getText().toString()) < 0.5) {
                                parcelBreadth.setError("Length cannot be less than 10 cms");
                            }
                        }
                        if (!parcelHeight.getText().toString().isEmpty()) {
                            if (Integer.parseInt(parcelHeight.getText().toString()) < 0.5) {
                                parcelHeight.setError("Length cannot be less than 10 cms");
                            }
                        }
                    }
                } else {
                    if (!deliveryLayout.isExpanded()) {
                        deliveryLayout.expand();
                    }
                    if (toName.getText().toString().isEmpty()) {
                        toName.setError("Required*");
                    }
                    if (toNumber.getText().toString().isEmpty()) {
                        toNumber.setError("Required*");
                    }
                    if (toAddress.getText().toString().isEmpty()) {
                        toAddress.setError("Required*");
                    }
                    if (toPincode.getText().toString().isEmpty()) {
                        toPincode.setError("Required*");
                    }
                    if (cityPincode.getText().toString().isEmpty()) {
                        toPincode.setError("Invalid PinCode!");
                    }
                }
            } else {
                if (fromName.getText().toString().isEmpty()) {
                    fromName.setError("Required*");
                }
                if (fromNumber.getText().toString().isEmpty()) {
                    fromNumber.setError("Required*");
                }
                if (pickupFrom.getText().toString().isEmpty()) {
                    pickupFrom.setError("Required*");
                }
                if (pickupFromComment.getText().toString().isEmpty()) {
                    pickupFromComment.setError("Required*");
                }
                if (fromPincode.getText().toString().isEmpty()) {
                    fromPincode.setError("Invalid Pincode*");
                }
            }
        } else {
            if (saveAddressCheckbox.isChecked()) {
                Log.e("CHECKED", "SAVE ADDRESS");
                if (!fromName.getText().toString().isEmpty() && !fromNumber.getText().toString().isEmpty() && !pickupFrom.getText().toString().isEmpty() && !addressAlias.getText().toString().isEmpty() && !pickupFromComment.getText().toString().isEmpty() && !fromPincode.getText().toString().isEmpty() && !pickupCityPincode.getText().toString().isEmpty()) {
                    if (!toName.getText().toString().isEmpty() && !toNumber.getText().toString().isEmpty() && !toAddress.getText().toString().isEmpty() && !toPincode.getText().toString().isEmpty() && !cityPincode.getText().toString().isEmpty()) {
                        if (!parcelWeight.getText().toString().isEmpty() && !parcelLength.getText().toString().isEmpty() && !parcelBreadth.getText().toString().isEmpty() && !parcelHeight.getText().toString().isEmpty()) {
                            if (needInsurance.isChecked()) {
                                if (!productPrice.getText().toString().isEmpty() && Integer.parseInt(productPrice.getText().toString()) > 0) {

                                    if (toMobile != 10 || fromMobile != 10) {
                                        if (toMobile != 10)
                                            toNumber.setError("Mobile number invalid!");
                                        if (fromMobile != 10)
                                            fromNumber.setError("Mobile number invalid");
                                    } else {
                                        FROM_NAME = fromName.getText().toString();
                                        FROM_NUMBER = fromNumber.getText().toString();
                                        FROM_ADDRESS = pickupFrom.getText().toString();
                                        ADDRESS_COMMENT = pickupFromComment.getText().toString();
                                        ADDRESS_ALIAS = addressAlias.getText().toString();
                                        TO_NAME = toName.getText().toString();
                                        TO_NUMBER = toNumber.getText().toString();
                                        TO_ADDRESS = toAddress.getText().toString();
                                        TO_PINCODE = toPincode.getText().toString();
                                        FROM_PINCODE = fromPincode.getText().toString();
                                        LENGTH = parcelLength.getText().toString();
                                        BREADTH = parcelBreadth.getText().toString();
                                        HEIGHT = parcelHeight.getText().toString();
                                        WEIGHT = parcelWeight.getText().toString();
                                        PRICE = productPrice.getText().toString();

                                        new showCouriers().execute();
                                    }
                                } else {
                                    productPrice.setError("Required and should be greater than 0");
                                }
                            } else {
                                if (toMobile != 10 || fromMobile != 10) {
                                    if (toMobile != 10)
                                        toNumber.setError("Mobile number invalid!");
                                    if (fromMobile != 10)
                                        fromNumber.setError("Mobile number invalid");
                                } else {
                                    FROM_NAME = fromName.getText().toString();
                                    FROM_NUMBER = fromNumber.getText().toString();
                                    FROM_ADDRESS = pickupFrom.getText().toString();
                                    ADDRESS_COMMENT = pickupFromComment.getText().toString();
                                    ADDRESS_ALIAS = addressAlias.getText().toString();
                                    TO_NAME = toName.getText().toString();
                                    TO_NUMBER = toNumber.getText().toString();
                                    TO_ADDRESS = toAddress.getText().toString();
                                    TO_PINCODE = toPincode.getText().toString();
                                    FROM_PINCODE = fromPincode.getText().toString();
                                    LENGTH = parcelLength.getText().toString();
                                    BREADTH = parcelBreadth.getText().toString();
                                    HEIGHT = parcelHeight.getText().toString();
                                    WEIGHT = parcelWeight.getText().toString();
                                    PRICE = "NOT AVAILABLE";

                                    new showCouriers().execute();
                                }
                            }
                        } else {
                            if (!productLayout.isExpanded()) {
                                productLayout.expand();
                            }
                            if (parcelWeight.getText().toString().isEmpty()) {
                                parcelWeight.setError("Required*");
                            }
                            if (parcelLength.getText().toString().isEmpty()) {
                                parcelLength.setError("Required*");
                            }
                            if (parcelBreadth.getText().toString().isEmpty()) {
                                parcelBreadth.setError("Required*");
                            }

                            if (parcelHeight.getText().toString().isEmpty()) {
                                parcelHeight.setError("Required*");
                            }
                            if (parcelWeight.getText().toString().equals("0")) {
                                parcelWeight.setError("Weight cannot be 0 kg!");
                            }
                            if (!parcelLength.getText().toString().isEmpty()) {
                                Log.e("LENGTH", parcelLength.getText().toString());
                                if (Integer.parseInt(parcelLength.getText().toString()) < 0.5) {
                                    parcelLength.setError("Length cannot be less than 10 cms");
                                }
                            }
                            if (!parcelBreadth.getText().toString().isEmpty()) {
                                if (Integer.parseInt(parcelBreadth.getText().toString()) < 0.5) {
                                    parcelBreadth.setError("Length cannot be less than 10 cms");
                                }
                            }
                            if (!parcelHeight.getText().toString().isEmpty()) {
                                if (Integer.parseInt(parcelHeight.getText().toString()) < 0.5) {
                                    parcelHeight.setError("Length cannot be less than 10 cms");
                                }
                            }
                        }
                    } else {
                        if (!deliveryLayout.isExpanded()) {
                            deliveryLayout.expand();
                        }
                        if (toName.getText().toString().isEmpty()) {
                            toName.setError("Required*");
                        }
                        if (toNumber.getText().toString().isEmpty()) {
                            toNumber.setError("Required*");
                        }
                        if (toAddress.getText().toString().isEmpty()) {
                            toAddress.setError("Required*");
                        }
                        if (toPincode.getText().toString().isEmpty()) {
                            toPincode.setError("Required*");
                        }
                        if (cityPincode.getText().toString().isEmpty()) {
                            toPincode.setError("Invalid PinCode!");
                        }
                        if (toMobile != 10) {
                            Log.e("ehhh", "correct");
                            toNumber.setError("Mobile Number Invalid!*");
                        } else {
                            Log.e("ehhh", "false");
                        }
                    }
                } else {
                    if (fromName.getText().toString().isEmpty()) {
                        fromName.setError("Required*");
                    }
                    if (fromNumber.getText().toString().isEmpty()) {
                        fromNumber.setError("Required*");
                    }
                    if (pickupFrom.getText().toString().isEmpty()) {
                        pickupFrom.setError("Required*");
                    }
                    if (pickupFromComment.getText().toString().isEmpty()) {
                        pickupFromComment.setError("Required*");
                    }
                    if (fromPincode.getText().toString().isEmpty()) {
                        fromPincode.setError("Invalid Pincode!");
                    }
                    if (addressAlias.getText().toString().isEmpty()) {
                        addressAlias.setError("Required*");
                    }
                    if (fromMobile != 10) {
                        Log.e("ehhh", "correct");
                        fromNumber.setError("Mobile Number Invalid!*");
                    } else {
                        Log.e("ehhh", "false");
                    }
                }
            } else {
                Log.e("NOT CHECKED", "SAVE ADDRESS");
                if (!fromName.getText().toString().isEmpty() && !fromNumber.getText().toString().isEmpty() && !pickupFrom.getText().toString().isEmpty() && !pickupFromComment.getText().toString().isEmpty() && !fromPincode.getText().toString().isEmpty() && !pickupCityPincode.getText().toString().isEmpty()) {
                    if (!toName.getText().toString().isEmpty() && !toNumber.getText().toString().isEmpty() && !toAddress.getText().toString().isEmpty() && !toPincode.getText().toString().isEmpty() && !cityPincode.getText().toString().isEmpty()) {
                        if (!parcelWeight.getText().toString().isEmpty() && !parcelLength.getText().toString().isEmpty() && !parcelBreadth.getText().toString().isEmpty() && !parcelHeight.getText().toString().isEmpty()) {
                            if (needInsurance.isChecked()) {
                                if (!productPrice.getText().toString().isEmpty() && Integer.parseInt(productPrice.getText().toString()) > 0) {

                                    if (toMobile != 10 || fromMobile != 10) {
                                        if (toMobile != 10)
                                            toNumber.setError("Mobile number invalid!");
                                        if (fromMobile != 10)
                                            fromNumber.setError("Mobile number invalid");
                                    } else {
                                        FROM_NAME = fromName.getText().toString();
                                        FROM_NUMBER = fromNumber.getText().toString();
                                        FROM_ADDRESS = pickupFrom.getText().toString();
                                        ADDRESS_COMMENT = pickupFromComment.getText().toString();
                                        ADDRESS_ALIAS = "NOT AVAILABLE";
                                        TO_NAME = toName.getText().toString();
                                        TO_NUMBER = toNumber.getText().toString();
                                        TO_ADDRESS = toAddress.getText().toString();
                                        TO_PINCODE = toPincode.getText().toString();
                                        FROM_PINCODE = fromPincode.getText().toString();
                                        LENGTH = parcelLength.getText().toString();
                                        BREADTH = parcelBreadth.getText().toString();
                                        HEIGHT = parcelHeight.getText().toString();
                                        WEIGHT = parcelWeight.getText().toString();
                                        PRICE = productPrice.getText().toString();

                                        new showCouriers().execute();
                                    }
                                } else {
                                    productPrice.setError("Required*");
                                }
                            } else {
                                if (toMobile != 10 || fromMobile != 10) {
                                    if (toMobile != 10)
                                        toNumber.setError("Mobile number invalid!");
                                    if (fromMobile != 10)
                                        fromNumber.setError("Mobile number invalid");
                                } else {
                                    FROM_NAME = fromName.getText().toString();
                                    FROM_NUMBER = fromNumber.getText().toString();
                                    FROM_ADDRESS = pickupFrom.getText().toString();
                                    ADDRESS_COMMENT = pickupFromComment.getText().toString();
                                    ADDRESS_ALIAS = "NOT AVAILABLE";
                                    TO_NAME = toName.getText().toString();
                                    TO_NUMBER = toNumber.getText().toString();
                                    TO_ADDRESS = toAddress.getText().toString();
                                    TO_PINCODE = toPincode.getText().toString();
                                    FROM_PINCODE = fromPincode.getText().toString();
                                    LENGTH = parcelLength.getText().toString();
                                    BREADTH = parcelBreadth.getText().toString();
                                    HEIGHT = parcelHeight.getText().toString();
                                    WEIGHT = parcelWeight.getText().toString();
                                    PRICE = "NOT AVAILABLE";

                                    new showCouriers().execute();
                                }
                            }
                        } else {
                            if (!productLayout.isExpanded()) {
                                productLayout.expand();
                            }
                            if (parcelWeight.getText().toString().isEmpty()) {
                                parcelWeight.setError("Required*");
                            }
                            if (parcelLength.getText().toString().isEmpty()) {
                                parcelLength.setError("Required*");
                            }
                            if (parcelBreadth.getText().toString().isEmpty()) {
                                parcelBreadth.setError("Required*");
                            }
                            if (parcelHeight.getText().toString().isEmpty()) {
                                parcelHeight.setError("Required*");
                            }
                            if (parcelWeight.getText().toString().equals("0")) {
                                parcelWeight.setError("Weight cannot be 0 kg!");
                            }
                            if (!parcelLength.getText().toString().isEmpty()) {
                                if (Integer.parseInt(parcelLength.getText().toString()) < 0.5) {
                                    parcelLength.setError("Length cannot be less than 10 cms");
                                }
                            }
                            if (!parcelBreadth.getText().toString().isEmpty()) {
                                if (Integer.parseInt(parcelBreadth.getText().toString()) < 0.5) {
                                    parcelBreadth.setError("Length cannot be less than 10 cms");
                                }
                            }
                            if (!parcelHeight.getText().toString().isEmpty()) {
                                if (Integer.parseInt(parcelHeight.getText().toString()) < 0.5) {
                                    parcelHeight.setError("Length cannot be less than 10 cms");
                                }
                            }
                        }
                    } else {
                        if (!deliveryLayout.isExpanded()) {
                            deliveryLayout.expand();
                        }
                        if (toName.getText().toString().isEmpty()) {
                            toName.setError("Required*");
                        }
                        if (toNumber.getText().toString().isEmpty()) {
                            toNumber.setError("Required*");
                        }
                        if (toAddress.getText().toString().isEmpty()) {
                            toAddress.setError("Required*");
                        }
                        if (toPincode.getText().toString().isEmpty()) {
                            toPincode.setError("Required*");
                        }
                        if (cityPincode.getText().toString().isEmpty()) {
                            toPincode.setError("Invalid PinCode!");
                        }
                        if (toMobile != 10) {
                            Log.e("ehhh", "correct");
                            toNumber.setError("Mobile Number Invalid!*");
                        } else {
                            Log.e("ehhh", "false");
                        }
                    }
                } else {
                    if (fromName.getText().toString().isEmpty()) {
                        fromName.setError("Required*");
                    }
                    if (fromNumber.getText().toString().isEmpty()) {
                        fromNumber.setError("Required*");
                    }
                    if (pickupFrom.getText().toString().isEmpty()) {
                        pickupFrom.setError("Required*");
                    }
                    if (pickupFromComment.getText().toString().isEmpty()) {
                        pickupFromComment.setError("Required*");
                    }
                    if (fromPincode.getText().toString().isEmpty()) {
                        fromPincode.setError("Invalid Pincode!");
                    }
                    if (fromMobile != 10) {
                        Log.e("ehhh", "correct");
                        fromNumber.setError("Mobile Number Invalid!*");
                    } else {
                        Log.e("ehhh", "false");
                    }
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class showCouriers extends AsyncTask {

        private ProgressDialog p;
        String message = "";
        JSONObject jsonObject;
        String courierResponse;
        String shipment_id = "";

        @Override
        protected void onPreExecute() {
            p = new ProgressDialog(CreateOrder.this);
            p.setMessage("Fetching Couriers...");
            p.setIndeterminate(false);
            p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p.setCancelable(false);
            p.show();
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("fromName", FROM_NAME)
                    .addFormDataPart("fromPincode", FROM_PINCODE)
                    .addFormDataPart("toPincode", TO_PINCODE)
                    .addFormDataPart("weight", WEIGHT)
                    .addFormDataPart("length", LENGTH)
                    .addFormDataPart("breadth", BREADTH)
                    .addFormDataPart("height", HEIGHT)
                    .addFormDataPart("price", PRICE)
                    .build();
            Request request = new Request.Builder().url("https://app.vourier.com/API_Layer.php").post(requestBody).build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (response.body() != null) {
                    try {
                        courierResponse = response.body().string();
                        Log.e("CourierResponse",courierResponse);
                        jsonObject = new JSONObject(courierResponse);
                        courierResponse = jsonObject.getString("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        message = jsonObject.getString("errors");
                    }
                    if (courierResponse.equals("200")) {
                        message = "OK";
                        JSONObject response_data = jsonObject.getJSONObject("data");
                        JSONArray couriers_available = response_data.getJSONArray("available_courier_companies");

                        if (needInsurance.isChecked()) {
                            MONEY_TO_ADD = MONEY_TO_ADD + (0.02 * Double.parseDouble(PRICE));
                    }

                        if (needPackaging.isChecked()) {
                            if (envelopeButton.isChecked()) {
                                MONEY_TO_ADD = MONEY_TO_ADD + 5;
                            } else if (packageButton.isChecked()) {
                                MONEY_TO_ADD = MONEY_TO_ADD + 10;
                            }
                        }

                        for (int i = 0; i < couriers_available.length(); i++) {
                            JSONObject jsonObject = couriers_available.getJSONObject(i);

                            String courierName = jsonObject.getString("courier_name");
                            String courierPrice = jsonObject.getString("rate");
                            String courierID = jsonObject.getString("courier_company_id");

                            courier_details.put(courierName, "₹" + (Double.parseDouble(courierPrice) + MONEY_TO_ADD) + "@" + courierID);
                        }
                        MONEY_TO_SUBTRACT = MONEY_TO_ADD;
                        MONEY_TO_ADD = MainActivity.MONEY_TO_ADD;
                    } else if (courierResponse.equals("404")) {
                        message = "PostCode is not serviceable by ShipDots";
                    } else {
                        message = "Some Error Occurred!";
                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                message = "Some Error Occurred!";
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AsyncTask() {

                        ProgressDialog progressDialog;
                        String insured, packing;

                        @Override
                        protected void onPreExecute() {
                            progressDialog = new ProgressDialog(CreateOrder.this);
                            progressDialog.setMessage("Adding Order");
                            progressDialog.setIndeterminate(false);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            super.onPreExecute();
                        }

                        @Override
                        protected Object doInBackground(Object[] objects) {

                            if(needInsurance.isChecked()) {
                                insured = "INSURED";
                            }
                            else {
                                insured = "NOT INSURED";
                            }

                            if(needPackaging.isChecked()) {
                                if(envelopeButton.isChecked()) {
                                    packing = "PACKING-ENVELOPE";
                                }
                                else {
                                    packing = "PACKING-BOX";
                                }
                            }
                            else {
                                packing = "NO PACKING";
                            }

                            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
                            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                                    .addFormDataPart("userID", final_id)
                                    .addFormDataPart("fromName", FROM_NAME)
                                    .addFormDataPart("fromNumber", FROM_NUMBER)
                                    .addFormDataPart("fromAddress", FROM_ADDRESS)
                                    .addFormDataPart("fromAddressComment", ADDRESS_COMMENT)
                                    .addFormDataPart("longitude", Longitude)
                                    .addFormDataPart("latitude", Latitude)
                                    .addFormDataPart("fromPincode", FROM_PINCODE)

                                    .addFormDataPart("addressAlias", ADDRESS_ALIAS)
                                    .addFormDataPart("toName", TO_NAME)
                                    .addFormDataPart("toNumber", TO_NUMBER)
                                    .addFormDataPart("toAddress", TO_ADDRESS)
                                    .addFormDataPart("toPincode", TO_PINCODE)

                                    .addFormDataPart("weight", WEIGHT)
                                    .addFormDataPart("length", LENGTH)
                                    .addFormDataPart("breadth", BREADTH)
                                    .addFormDataPart("height", HEIGHT)
                                    .addFormDataPart("price", PRICE)
                                    .addFormDataPart("HUB_ID", HUB_ID)
                                    .addFormDataPart("DETAILS", insured+"/"+packing)
                                    .build();
                            Log.e("HUB_ID", HUB_ID);
                            Request request = new Request.Builder().url("https://app.vourier.com/API/create_order.php").post(requestBody).build();
                            try {
                                Response response = okHttpClient.newCall(request).execute();
                                if (response.body() != null) {
                                    try {
                                        courierResponse = response.body().string();

                                        JSONObject responseFromServer = new JSONObject(courierResponse);
                                        String responseStatus = responseFromServer.getString("status");

                                        switch (responseStatus) {
                                            case "NEW":
                                                message = "Order Successfully Added";
                                                shipment_id = responseFromServer.getString("shipment_id");
                                                break;
                                            case "202":
                                                message = "Order Added but Address not saved.";
                                                shipment_id = responseFromServer.getString("shipment_id");
                                                break;
                                            case "404":
                                                message = "Server is down. Please try again.";
                                                break;
                                            default:
                                                message = "Process Failed! Try again";
                                                break;
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();

                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            progressDialog.dismiss();
                            final ConstraintLayout constraintLayout = findViewById(R.id.constraint_layout_create_order);
                            if (!message.equals("OK")) {
                                Snackbar.make(constraintLayout, message, Snackbar.LENGTH_LONG).show();
                            }
                            super.onPostExecute(o);
                        }

                    }.execute();
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            p.dismiss();
            final ConstraintLayout constraintLayout = findViewById(R.id.constraint_layout_create_order);
            if (!message.equals("OK")) {
                Snackbar.make(constraintLayout, message, Snackbar.LENGTH_LONG).show();
            } else {
                LayoutInflater layoutInflater = LayoutInflater.from(CreateOrder.this);
                @SuppressLint("InflateParams") final View courierListView = layoutInflater.inflate(R.layout.available_couriers, null);

                final ListView courierList = courierListView.findViewById(R.id.courier_list);


                HashMapAdapter hashMapAdapter = new HashMapAdapter(courier_details);
                courierList.setAdapter(hashMapAdapter);

                


                courierList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        final String[] name = courierList.getItemAtPosition(i).toString().split("=");
                        final String[] priceAndID = name[1].split("@");


                        double insur = 0.0;
                        double pack = 0.0;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (needInsurance.isChecked()) {
                            insur = 0.02 * Double.parseDouble(PRICE);
                        }
                        if(needPackaging.isChecked()) {
                            if (envelopeButton.isChecked()) {
                                pack = 5.0;
                            } else if (packageButton.isChecked()) {
                                pack = 10.0;
                            }
                        }


                        listItem = getResources().getStringArray(R.array.Final_Revised_Payment);
                        checkedItems = new boolean[listItem.length];

                        new AlertDialog.Builder(new ContextThemeWrapper(CreateOrder.this, R.style.myDialog))
                                .setTitle("Final Receipt")
                                .setMessage("Shipping with " + name[0] + " at a cost of " + priceAndID[0] + " \n\n Courier Price = " + (Double.parseDouble(priceAndID[0].substring(1)) - (insur + pack)) + "\n Insurance Price = ₹" + insur + "\n Packaging Price = ₹" + pack)
                                .setMultiChoiceItems(listItem, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                                        if(isChecked){
                                            if(!mUserItems.contains(position)){
                                                mUserItems.add(position);
                                            }else{
                                                mUserItems.remove(position);
                                            }
                                        }

                                    }
                                })
                                .setPositiveButton("Pay Online", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        String final_selection ="";

                                        for(int item=0;item < mUserItems.size();item++){
                                            final_selection = final_selection + listItem[mUserItems.get(item)] ;

                                        }
                                        Toast.makeText(CreateOrder.this, priceAndID[1], Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .setNegativeButton("Pay On Pickup", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(final DialogInterface dialogInterface, int i) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                new AsyncTask() {

                                                    @Override
                                                    protected void onPreExecute() {
                                                        p = new ProgressDialog(CreateOrder.this);
                                                        p.setMessage("Confirming Order");
                                                        p.setIndeterminate(false);
                                                        p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                                        p.setCancelable(false);
                                                        p.show();
                                                        super.onPreExecute();
                                                    }

                                                    @Override
                                                    protected Object doInBackground(Object[] objects) {
                                                        Log.e("ENDING", "ACTIVITY");
                                                        return null;
                                                    }

                                                    @Override
                                                    protected void onPostExecute(Object o) {
                                                        p.dismiss();
                                                        courierListView.setVisibility(View.GONE);
                                                        dialogInterface.cancel();
                                                        final ConstraintLayout constraintLayout = findViewById(R.id.constraint_layout_create_order);
                                                        if (!message.equals("OK")) {
                                                            Snackbar.make(constraintLayout, message, Snackbar.LENGTH_LONG).show();
                                                        }
                                                        String realPrice = String.valueOf(Double.parseDouble(priceAndID[0].substring(1)) - MONEY_TO_SUBTRACT);

                                                        startActivity(new Intent(CreateOrder.this, ConfirmOrder.class)
                                                                .putExtra("SHIPMENT_ID", shipment_id)
                                                                .putExtra("PRICE_BY_CUSTOMER", priceAndID[0].substring(1))
                                                                .putExtra("PRICE_BY_SHIPROCKET", realPrice)
                                                                .putExtra("COURIER_ID", priceAndID[1])
                                                                .putExtra("COURIER_NAME", name[0]));
                                                        finish();
                                                        super.onPostExecute(o);
                                                    }
                                                }.execute();
                                            }
                                        });
                                    }
                                })
                                .show();

                    }
                });

                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(CreateOrder.this);
                alertDialogBuilder.setView(courierListView);

                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            super.onPostExecute(o);
        }
    }

    public void vibrate() {
        Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(100);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class getAliasDetails extends AsyncTask<String, Void, Void> {

        private ProgressDialog progressDialog;
        HashMap<String, String> aliasDetails = new HashMap<>();

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(CreateOrder.this);
            progressDialog.setMessage("Syncing..");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {

            String alias = strings[0];

            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            //StrictMode.setThreadPolicy(policy);
            try {
                //Class.forName("com.mysql.jdbc.Driver");
                 //Connection conn = DriverManager.getConnection(SplashScreen.url, SplashScreen.db_username, SplashScreen.db_password);
                //Statement st = conn.createStatement();
                if(conn!=null) {
                   /* if (conn.isClosed()) {
                        conn = DriverManager.getConnection(SplashScreen.url, db_username, db_password);
                        stmt = conn.createStatement();
                    }*/
                    ResultSet rs = stmt.executeQuery("SELECT * FROM customer_address_mobile WHERE Address_Email = '" + final_id + "' AND Address_Alias = '" + alias + "' ");
                    if (rs.next()) {
                        rs.beforeFirst();
                        while (rs.next()) {
                            aliasDetails.put("ADDRESS_NAME", rs.getString("Address_Name"));
                            aliasDetails.put("ADDRESS_NUMBER", rs.getString("Address_Mobile"));
                            aliasDetails.put("ADDRESS_LINE_1", rs.getString("Address_Line_1"));
                            aliasDetails.put("ADDRESS_LINE_2", rs.getString("Address_Line_2"));
                            aliasDetails.put("ADDRESS_PINCODE", rs.getString("Address_Pincode"));
                            aliasDetails.put("ADDRESS_LATITUDE", rs.getString("Address_Latitude"));
                            aliasDetails.put("ADDRESS_LONGITUDE", rs.getString("Address_Longitude"));
                        }
                    }
                }
               //conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            pickupLayout.expand();
            progressDialog.dismiss();
            fromName.setText(aliasDetails.get("ADDRESS_NAME"));
            fromNumber.setText(aliasDetails.get("ADDRESS_NUMBER"));
            pickupFrom.setText(aliasDetails.get("ADDRESS_LINE_2"));
            pickupFromComment.setText(aliasDetails.get("ADDRESS_LINE_1"));
            fromPincode.setText(aliasDetails.get("ADDRESS_PINCODE"));
            Latitude = aliasDetails.get("ADDRESS_LATITUDE");
            Longitude = aliasDetails.get("ADDRESS_LONGITUDE");

            super.onPostExecute(aVoid);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class getCity extends AsyncTask<String, Void, Void> {

        private ProgressDialog p;
        String city = "";

        @Override
        protected void onPreExecute() {
            p = new ProgressDialog(CreateOrder.this);
            p.setMessage("Validating Pincode");
            p.setIndeterminate(false);
            p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p.setCancelable(false);
            if(p!=null && p.isShowing())
            p.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {

            final String type = strings[0];
            final String code = strings[1];

            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            //StrictMode.setThreadPolicy(policy);
            try {
               //Class.forName("com.mysql.jdbc.Driver");
               //Connection conn = DriverManager.getConnection(SplashScreen.url, SplashScreen.db_username, SplashScreen.db_password);
                //final Statement stmt = conn.createStatement();

               /* if(conn.isClosed()) {
                    conn = DriverManager.getConnection(SplashScreen.url, db_username, db_password);
                    //stmt = conn.createStatement();
                }

                */

                ResultSet rs = stmt.executeQuery("SELECT * FROM pincode_database WHERE pin_code = '" + code + "' ");
                if (rs.next()) {
                    rs.beforeFirst();
                        while (rs.next()) {
                            city = rs.getString("City");

                            if (type.equals("TO")) {
                                cityPincode.setText(city);
                            }
                            else {
                                HUB_ID = rs.getString("HUB_ID");
                                if(!HUB_ID.isEmpty()) {
                                    if(type.equals("FROM")) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                new AsyncTask() {

                                                    ProgressDialog distanceCl;

                                                    @Override
                                                    protected void onPreExecute() {
                                                        distanceCl = new ProgressDialog(CreateOrder.this);
                                                        distanceCl.setMessage("Calculating Distance..");
                                                        distanceCl.setIndeterminate(false);
                                                        distanceCl.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                                        distanceCl.setCancelable(false);
                                                        if(distanceCl!=null && distanceCl.isShowing())
                                                        distanceCl.show();
                                                        super.onPreExecute();
                                                    }

                                                    @Override
                                                    protected Object doInBackground(Object[] objects) {
                                                        try {
                                                          //Connection newConn = DriverManager.getConnection(SplashScreen.url, SplashScreen.db_username, SplashScreen.db_password);
                                                            //Statement st = newConn.createStatement();



                                                           /* if(conn.isClosed()) {
                                                                conn = DriverManager.getConnection(SplashScreen.url, db_username, db_password);
                                                                //stmt = conn.createStatement();
                                                            }

                                                            */
                                                            Statement st = conn.createStatement();
                                                            ResultSet newSet = st.executeQuery("SELECT * FROM hub_details WHERE HUB_ID = '"+HUB_ID+"'");
                                                            if(newSet.next()) {
                                                                newSet.beforeFirst();
                                                                while (newSet.next()) {
                                                                    String HubPincode = newSet.getString("HUB_PINCODE");
                                                                    geocoder = new Geocoder(CreateOrder.this, Locale.getDefault());
                                                                    try {
                                                                        hubLat = geocoder.getFromLocationName(HubPincode, 1).get(0).getLatitude();
                                                                        hubLang = geocoder.getFromLocationName(HubPincode, 1).get(0).getLongitude();

                                                                        codeLat = geocoder.getFromLocationName(code, 1).get(0).getLatitude();
                                                                        codeLang = geocoder.getFromLocationName(code, 1).get(0).getLongitude();


                                                                        double theta = hubLang - codeLang;
                                                                        double dist = Math.sin(Math.toRadians(hubLat)) * Math.sin(Math.toRadians(codeLat)) + Math.cos(Math.toRadians(hubLat)) * Math.cos(Math.toRadians(codeLat)) * Math.cos(Math.toRadians(theta));
                                                                        dist = Math.acos(dist);
                                                                        dist = Math.toDegrees(dist);
                                                                        dist = dist * 60 * 1.1515;
                                                                        dist = dist * 1.609344;

                                                                        Log.e("DISTANCE", String.valueOf(dist));


                                                                        if(dist < 25) {

                                                                            runOnUiThread(new Runnable() {
                                                                                @Override
                                                                                public void run() {
                                                                                    if (type.equals("FROM"))
                                                                                        pickupCityPincode.setText(city);
                                                                                    else
                                                                                        cityPincode.setText(city);
                                                                                }
                                                                            });

                                                                        }
                                                                        else {
                                                                            runOnUiThread(new Runnable() {
                                                                                @Override
                                                                                public void run() {

                                                                                    if (type.equals("FROM")) {
                                                                                        pickupCityPincode.setText("");
                                                                                        fromPincode.setError("Pincode not serviceable!");
                                                                                    } else {
                                                                                        cityPincode.setText("");
                                                                                        toPincode.setError("Pincode not serviceable!");
                                                                                    }
                                                                                    Toast.makeText(CreateOrder.this, "Pincode not serviceable!", Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            });
                                                                         /*   if (type.equals("FROM")) {
                                                                                pickupCityPincode.setText("");
                                                                                fromPincode.setError("Pincode not serviceable!");
                                                                            } else {
                                                                                cityPincode.setText("");
                                                                                toPincode.setError("Pincode not serviceable!");
                                                                            } */
                                                                        }
                                                                    }catch (WindowManager.BadTokenException ex) {
                                                                    ex.printStackTrace();
                                                                }
                                                                    catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                            else {
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {

                                                                        if (type.equals("FROM")) {
                                                                            pickupCityPincode.setText("");
                                                                            fromPincode.setError("Pincode not serviceable!");
                                                                        } else {
                                                                            cityPincode.setText("");
                                                                            toPincode.setError("Pincode not serviceable!");
                                                                        }

                                                                        Toast.makeText(CreateOrder.this, "Pincode not serviceable!", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });
                                                               /* if (type.equals("FROM")) {
                                                                    pickupCityPincode.setText("");
                                                                    fromPincode.setError("Pincode not serviceable!");
                                                                } else {
                                                                    cityPincode.setText("");
                                                                    toPincode.setError("Pincode not serviceable!");
                                                                }*/
                                                            }
                                                            //conn.close();
                                                        } catch (WindowManager.BadTokenException ex) {
                                                            ex.printStackTrace();
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                        }
                                                        return null;
                                                    }

                                                    @Override
                                                    protected void onPostExecute(Object o) {
                                                        distanceCl.dismiss();
                                                        super.onPostExecute(o);
                                                    }
                                                }.execute();
                                            }
                                        });
                                    }
                                }
                                else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (type.equals("FROM")) {
                                                pickupCityPincode.setText("");
                                                fromPincode.setError("Pincode not serviceable!");
                                            } else {
                                                cityPincode.setText("");
                                                toPincode.setError("Pincode not serviceable!");
                                            }
                                            Toast.makeText(CreateOrder.this, "Pincode not serviceable!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                  /*  if (type.equals("FROM")) {
                                        pickupCityPincode.setText("");
                                        fromPincode.setError("Pincode not serviceable!");
                                    } else {
                                        cityPincode.setText("");
                                        toPincode.setError("Pincode not serviceable!");
                                    }*/
                                }
                            }
                        }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (type.equals("FROM")) {
                                pickupCityPincode.setText("");
                                fromPincode.setError("Invalid Pincode!");
                            } else {
                                cityPincode.setText("");
                                toPincode.setError("Invalid Pincode!");
                            }
                            Toast.makeText(CreateOrder.this, "Invalid Pincode!", Toast.LENGTH_SHORT).show();
                        }
                    });
                   /* if (type.equals("FROM")) {
                        pickupCityPincode.setText("");
                        fromPincode.setError("Invalid Pincode!");
                    } else {
                        cityPincode.setText("");
                        toPincode.setError("Invalid Pincode!");
                    }*/
                }
                //conn.close();
            } catch (WindowManager.BadTokenException ex) {
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            p.dismiss();
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }
}
