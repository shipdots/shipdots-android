package akshay.tech.com.shipdots.OtherTools;

public class Property {

    //property basics
    private int IMAGE_RESOURCE;
    private String OrderID;
    private String courierName;
    private String awbNumber;
    private String paymentMethod;
    private String price;
    private String status;
    private String receiverName;

    //constructor
    public Property(int IMAGE_RESOURCE, String OrderID, String courierName, String awbNumber, String paymentMethod, String price, String status, String receiverName){

        this.IMAGE_RESOURCE = IMAGE_RESOURCE;
        this.OrderID = OrderID;
        this.courierName = courierName;
        this.awbNumber = awbNumber;
        this.paymentMethod = paymentMethod;
        this.price = price;
        this.status = status;
        this.receiverName = receiverName;
    }

    //getters
    int getIMAGE_RESOURCE() { return IMAGE_RESOURCE; }
    public String getOrderID() { return OrderID; }
    String getcourierName() {return courierName; }
    String getawbNumber() {return awbNumber; }
    String getpaymentMethod() {return paymentMethod; }
    String getstatus() {return status; }
    String getPrice() {return price; }
    String getReceiverName() {return receiverName; }
}
