package akshay.tech.com.shipdots.OtherTools;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import java.util.Objects;

import akshay.tech.com.shipdots.R;

public class TrackWebView extends AppCompatActivity {

    WebView webView;
    String COURIER_NAME, AWB_NUMBER;
    ProgressBar progressBar;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_web_view);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Order Tracking");

        COURIER_NAME = getIntent().getStringExtra("COURIER NAME");
        AWB_NUMBER = getIntent().getStringExtra("AWB NUMBER");

        progressBar = findViewById(R.id.progressBar);

        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClientClass(progressBar));
        webView.setInitialScale(1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);

        switch (COURIER_NAME) {
            case "Fedex":
                webView.loadUrl("https://www.fedex.com/apps/fedextrack/index.html?tracknumbers=" + AWB_NUMBER + "&cntry_code=in");
                break;
            case "Xpressbees":
                webView.loadUrl("http://www.xpressbees.com/track-shipment.aspx?tracking_id=" + AWB_NUMBER);
                break;
            case "Dotzot":
                webView.loadUrl("https://instacom.dotzot.in/GUI/Tracking/Track.aspx");
                break;
            case "Delhivery":
                webView.loadUrl("https://www.delhivery.com/");
                break;
            case "Bluedart":
                webView.loadUrl("https://www.bluedart.com/web/guest/trackdartresult");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), Orders.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    public class WebViewClientClass extends android.webkit.WebViewClient {

        private ProgressBar progressBar;

        WebViewClientClass(ProgressBar progressBar) {
            this.progressBar = progressBar;
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }
    }
}
