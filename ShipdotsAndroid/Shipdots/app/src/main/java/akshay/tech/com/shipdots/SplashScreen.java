package akshay.tech.com.shipdots;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.felipecsl.gifimageview.library.GifImageView;
import com.google.android.gms.common.util.IOUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SplashScreen extends AppCompatActivity {

    public static String url = "jdbc:mysql://app.vourier.com/logistics_v2?useSSL=false";
          //  +"?verifyServerCertificate=true"+
          //  "&useSSL=true"+
           // "&requireSSL=true";
    public static String db_username = "admin";
    public static String db_password = "Ankur123@";
    String Email, Password = "";
    File password_file = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/pw.txt");
    int counter = 0;
    GifImageView gifImageView;
    public static Connection conn;
    public static Statement stmt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);

        gifImageView = findViewById(R.id.splash_gif);

        try{
            InputStream inputStream = getAssets().open("vourier_splash.gif");
            byte[] bytes = IOUtils.toByteArray(inputStream);
            gifImageView.setBytes(bytes);
            gifImageView.startAnimation();

            new connection().execute();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }

        //CREATE PASSWORD FILE IF IT ISN'T THERE
        File folder = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/");
        if(!folder.exists()) {
            try {
                folder.mkdirs();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        //IF PASSWORD SAVED
        String[] details = checkForPasswordFile();
        if(details != null) {
            Email = details[0];
            Password = details[1];

            new LoginTask().execute();
        }
        else {
            startActivity(new Intent(SplashScreen.this, LoginScreen.class));
        }

    }




    private String[] checkForPasswordFile() {
        File passwordFile = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/pw.txt");
        String[] det_array = null;
        if(passwordFile.exists()) {
            String line;
            String details = "";
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(passwordFile)));
                while ((line = reader.readLine())!=null) {
                    details = line;
                }
                det_array = details.split(" ");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return det_array;
    }


    public static class connection  extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {

            try{
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(url,db_username,db_password);
                stmt = conn.createStatement();


            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class LoginTask extends AsyncTask {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            try {
               // Class.forName("com.mysql.jdbc.Driver");
                //conn = DriverManager.getConnection(url,db_username,db_password);
               // stmt = conn.createStatement();

                //String jordan = "SELECT * FROM customer_details WHERE Email = '"+Email+"' AND password ='"+Password+"'";
                ResultSet rs = stmt.executeQuery("SELECT * FROM customer_details WHERE Email = '"+Email+"' AND password ='"+Password+"'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        counter++;
                        //Log.e("The result is:", String.valueOf(rs));
                        //String st = rs.getString("Email");
                       // Log.e("E-MAIL ID is:",st);


                    }
                }
                else {
                    if(password_file.exists()) {
                        ConstraintLayout constraintLayout = findViewById(R.id.constraint_layout_login_screen);
                        if(password_file.delete()) {
                            Snackbar.make(constraintLayout, "ID or Password Changed!", Snackbar.LENGTH_LONG);
                            startActivity(new Intent(SplashScreen.this, LoginScreen.class));
                        }
                    }
                }

                //conn.close();
            }
            catch (Exception e) {

                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(counter > 0) {

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                ArrayList <String> addressDetails = new ArrayList<>();

                try {
                    addressDetails.add("Select Pickup Address*");
                    //Class.forName("com.mysql.jdbc.Driver");
                    // Connection conn = DriverManager.getConnection(url, db_username, db_password);
                    ResultSet rs = stmt.executeQuery("SELECT * FROM customer_address_mobile WHERE Address_Email = '"+Email+"' ");
                    if(rs.next()) {
                        rs.beforeFirst();
                        while(rs.next()) {
                            addressDetails.add( rs.getString("Address_Alias"));
                        }
                    }
                    //conn.close();

                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                intent.putExtra("ID", Email);
                intent.putExtra("PASSWORD", Password);
                intent.putExtra("ADDRESS_LIST", addressDetails);
                startActivity(intent);
            }
            super.onPostExecute(o);
        }
    }
}




