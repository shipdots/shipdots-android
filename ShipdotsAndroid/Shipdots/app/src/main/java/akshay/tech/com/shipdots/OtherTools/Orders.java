package akshay.tech.com.shipdots.OtherTools;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Objects;


import akshay.tech.com.shipdots.MainActivity;
import akshay.tech.com.shipdots.R;


import static akshay.tech.com.shipdots.MainActivity.final_id;
import static akshay.tech.com.shipdots.SplashScreen.conn;
import static akshay.tech.com.shipdots.SplashScreen.db_password;
import static akshay.tech.com.shipdots.SplashScreen.db_username;
import static akshay.tech.com.shipdots.SplashScreen.stmt;
import static akshay.tech.com.shipdots.SplashScreen.url;

public class Orders extends AppCompatActivity {

    public static ArrayList<Property> orderProperties = new ArrayList<>();
    public static ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("All Orders");

        listView = findViewById(R.id.order_list);

        new getRecentOrders().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    public class getRecentOrders extends AsyncTask {

        ProgressDialog progressDialog;
        int IMAGE_RESOURCE;
        String OrderID, courierName, awbNumber, paymentMethod, price, status, receiver_name = "";

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(Orders.this);
            progressDialog.setMessage("Fetching Order Details");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

          //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
         // StrictMode.setThreadPolicy(policy);

            try {
                // Class.forName("com.mysql.jdbc.Driver");
                //Connection conn = DriverManager.getConnection(SplashScreen.url,SplashScreen.db_username, SplashScreen.db_password);
                //Statement st = conn.createStatement();
                //ResultSet rs = st.executeQuery("SELECT Order_ID,operator,AWB_Number,Payment_Mode,Price_Paid_By_Customer,status,Customer_Name,COD FROM customer_orders WHERE Email ='"+final_id+"' ORDER BY timestamp DESC ");

                if (conn != null) {
                    //if (conn.isClosed())
                        //conn = DriverManager.getConnection(url, db_username, db_password);
                        //stmt = conn.createStatement();

                    String st = "SELECT * FROM customer_orders WHERE Email = '"+final_id+"' ORDER BY timestamp DESC ";
                    ResultSet rs = stmt.executeQuery(st);
                    if (rs.next()) {
                        rs.beforeFirst();
                        while (rs.next()) {
                            OrderID = rs.getString("Order_ID");
                            Log.e("orderid", OrderID);
                            courierName = rs.getString("operator");
                            awbNumber = rs.getString("AWB_Number");
                            paymentMethod = rs.getString("Payment_Mode");

                            // Log.e("Payment_Method",String.valueOf(paymentMethod));
                            if(rs.getString("Price_Paid_By_Customer")==null) {
                                price = "₹0";
                            }else {
                                price = "₹" + rs.getString("Price_Paid_By_Customer");
                            }
                            status = rs.getString("status");
                            receiver_name = rs.getString("Customer_Name");
                            String s = rs.getString("COD");

                            //Log.e("COD is here:",s);
                                if (courierName==null)
                                    courierName = "NOT ASSIGNED";


                                if (awbNumber==null) {
                                    awbNumber = "Not Assigned";
                                }


                                if (paymentMethod==null) {
                                    paymentMethod = rs.getString("COD");
                                    if (paymentMethod.equalsIgnoreCase("Yes"))
                                        paymentMethod = "POSTPAID";
                                    else
                                        paymentMethod = "PREPAID";
                                }



                                if (courierName.contains("Fedex") || courierName.contains("FEDEX-SURFACE")) {
                                    IMAGE_RESOURCE = R.drawable.fedex;
                                } else if (courierName.contains("Bluedart")) {
                                    IMAGE_RESOURCE = R.drawable.bluedart;
                                } else if (courierName.contains("Dotzot")) {
                                    IMAGE_RESOURCE = R.drawable.dotzot;
                                } else if (courierName.contains("Xpressbees")) {
                                    IMAGE_RESOURCE = R.drawable.xpressbees;
                                } else if (courierName.contains("Delhivery")) {
                                    IMAGE_RESOURCE = R.drawable.delhivery;
                                } else if (courierName.contains("DHL")) {
                                    IMAGE_RESOURCE = R.drawable.dhl;
                                } else if (courierName.contains("Ekart")) {
                                    IMAGE_RESOURCE = R.drawable.ekart;
                                } else {
                                    IMAGE_RESOURCE = R.drawable.box;
                                }


                            orderProperties.add(new Property(IMAGE_RESOURCE, OrderID, courierName, awbNumber, paymentMethod, price, status, receiver_name));
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Orders.this, "No orders!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    //conn.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            final ArrayAdapter<Property> adapter = new RecentOrderAdapter(Orders.this, 0, orderProperties);
            listView.setAdapter(adapter);
            progressDialog.dismiss();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Property property = orderProperties.get(i);

                    Log.e("ORDER-ID", property.getOrderID());
                }
            });
            super.onPostExecute(o);
        }
    }
}
