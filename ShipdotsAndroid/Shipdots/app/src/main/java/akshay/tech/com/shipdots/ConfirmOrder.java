package akshay.tech.com.shipdots;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.felipecsl.gifimageview.library.GifImageView;
import com.google.android.gms.common.util.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import akshay.tech.com.shipdots.OtherTools.Orders;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static akshay.tech.com.shipdots.SplashScreen.conn;
import static akshay.tech.com.shipdots.SplashScreen.db_password;
import static akshay.tech.com.shipdots.SplashScreen.db_username;
import static akshay.tech.com.shipdots.SplashScreen.stmt;
import static akshay.tech.com.shipdots.SplashScreen.url;

public class ConfirmOrder extends AppCompatActivity {

    String SHIPMENT_ID, PRICE_BY_CUSTOMER, PRICE_BY_SHIPROCKET, COURIER_ID, COURIER_NAME;
    GifImageView gifImageView;

    TextView OrderID, OrderDate, OrderStatus;
    TextView OrderLength, OrderBreadth, OrderHeight, OrderWeight, OrderPrice;
    TextView fromAddress, toAddress;
    TextView courierCompanyName, courierCompanyPrice, paymentMode;
    ImageView courierImage;
    Button recentOrderButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);

        SHIPMENT_ID = getIntent().getStringExtra("SHIPMENT_ID");
        PRICE_BY_CUSTOMER = getIntent().getStringExtra("PRICE_BY_CUSTOMER");
        PRICE_BY_SHIPROCKET = getIntent().getStringExtra("PRICE_BY_SHIPROCKET");
        COURIER_ID = getIntent().getStringExtra("COURIER_ID");
        COURIER_NAME = getIntent().getStringExtra("COURIER_NAME");

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Order Confirmation");

        gifImageView = findViewById(R.id.gifImageView);

        OrderID = findViewById(R.id.orderIDValue);
        OrderDate = findViewById(R.id.orderDateValue);
        OrderStatus = findViewById(R.id.orderStatusValue);

        OrderLength = findViewById(R.id.dimensions_length);
        OrderBreadth = findViewById(R.id.dimensions_breadth);
        OrderHeight = findViewById(R.id.dimensions_height);
        OrderWeight = findViewById(R.id.weight);
        OrderPrice = findViewById(R.id.price_value);

        fromAddress = findViewById(R.id.fromAddressValue);
        toAddress = findViewById(R.id.toAddressValue);

        courierCompanyName = findViewById(R.id.courierNameValue);
        courierCompanyPrice = findViewById(R.id.courierPriceValue);
        paymentMode = findViewById(R.id.paymentModeValue);

        courierImage = findViewById(R.id.courierImage);

        recentOrderButton = findViewById(R.id.recentOrderButton);

        recentOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ConfirmOrder.this, Orders.class));
                finish();
            }
        });

        new payAndConfirm().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    public class payAndConfirm extends AsyncTask {

        ProgressDialog progressDialog;
        String message = "";
        String serverResponse;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(ConfirmOrder.this);
            progressDialog.setMessage("Confirming Order");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("REQUEST_TYPE", "PAY_AND_CONFIRM")
                    .addFormDataPart("SHIPMENT_ID", SHIPMENT_ID)
                    .addFormDataPart("PRICE_BY_CUSTOMER", PRICE_BY_CUSTOMER)
                    .addFormDataPart("PRICE_BY_SHIPROCKET", PRICE_BY_SHIPROCKET)
                    .addFormDataPart("COURIER_ID", COURIER_ID)
                    .addFormDataPart("COURIER_NAME", COURIER_NAME)
                    .addFormDataPart("PAYMENT_MODE", "CASH")
                    .build();
            Request request = new Request.Builder().url("https://app.vourier.com/API/payAndConfirm.php").post(requestBody).build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if(response.body() != null) {
                    serverResponse = response.body().string();

                    Log.e("SERVER RESPONSE", serverResponse);

                    if (serverResponse.equals("200")) {
                        message = "Order Confirmed";
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AsyncTask() {
                                    ProgressDialog progress;
                                    String Order_ID, Order_Date, Order_Status, Order_Length, Order_Breadth, Order_Height, Order_Weight, Order_Declared_Value, Order_fromAddress, Order_toAddress, Order_courierName, Order_courierPrice, Order_Payment_Mode;

                                    @Override
                                    protected void onPreExecute() {
                                        progress = new ProgressDialog(ConfirmOrder.this);
                                        progress.setMessage("Fetching Order Details");
                                        progress.setIndeterminate(false);
                                        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                        progress.setCancelable(false);
                                        progress.show();
                                        super.onPreExecute();
                                    }

                                    @Override
                                    protected Object doInBackground(Object[] objects) {

                                       // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                        //StrictMode.setThreadPolicy(policy);

                                        try {
                                            //Class.forName("com.mysql.jdbc.Driver");
                                            // Connection conn = DriverManager.getConnection(LoginScreen.url, LoginScreen.db_username, LoginScreen.db_password);
                                            //Statement stmt = conn.createStatement();


                                            if(conn.isClosed()) {
                                                conn = DriverManager.getConnection(url, db_username, db_password);
                                                stmt = conn.createStatement();
                                            }
                                            ResultSet rs = stmt.executeQuery("SELECT * FROM customer_orders WHERE Shipment_ID ='"+SHIPMENT_ID+"'");
                                            if(rs.next()) {
                                                rs.beforeFirst();
                                                while(rs.next()) {
                                                    Order_ID = rs.getString("Order_ID");
                                                    Order_Date = rs.getString("order_date");
                                                    Order_Status = rs.getString("Status");
                                                    Order_Length = rs.getString("length");
                                                    Order_Breadth = rs.getString("breadth");
                                                    Order_Height = rs.getString("height");
                                                    Order_Weight = rs.getString("Weight");
                                                    Order_Declared_Value = rs.getString("Price");
                                                    Order_fromAddress = rs.getString("Sender_Name")+"\n"+rs.getString("sender_mobile")+"\n"+rs.getString("sender_address_line_1")+"\n"+rs.getString("sender_address_line_2")+"\n"+rs.getString("sender_pincode");
                                                    Order_toAddress = rs.getString("Customer_Name")+"\n"+rs.getString("customer_mobile")+"\n"+rs.getString("customer_address_line_1")+"\n"+rs.getString("customer_address_line_2")+"\n"+rs.getString("customer_pincode");
                                                    Order_courierName = rs.getString("operator");
                                                    Order_courierPrice = rs.getString("Price_Paid_By_Customer");
                                                    Order_Payment_Mode = rs.getString("Payment_Mode");
                                                }
                                            }
                                            else {
                                                Toast.makeText(ConfirmOrder.this, "No such order!", Toast.LENGTH_SHORT).show();
                                            }
                                           // conn.close();
                                        }
                                        catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        return null;
                                    }

                                    @SuppressLint("SetTextI18n")
                                    @Override
                                    protected void onPostExecute(Object o) {

                                        try
                                        {
                                            InputStream inputStream = getAssets().open("order_confirm.gif");
                                            byte[] bytes = IOUtils.toByteArray(inputStream);
                                            gifImageView.setBytes(bytes);
                                            gifImageView.startAnimation();
                                        }
                                        catch (IOException ex)
                                        {
                                            ex.printStackTrace();
                                        }

                                        OrderID.setText(Order_ID);
                                        OrderDate.setText(Order_Date);
                                        OrderStatus.setText(Order_Status);
                                        OrderLength.setText(Order_Length);
                                        OrderBreadth.setText(Order_Breadth);
                                        OrderHeight.setText(Order_Height);
                                        OrderWeight.setText(Order_Weight);
                                        OrderPrice.setText("₹"+Order_Declared_Value);
                                        fromAddress.setText(Order_fromAddress);
                                        toAddress.setText(Order_toAddress);
                                        courierCompanyName.setText(Order_courierName);

                                        int IMAGE_RESOURCE;
                                        if(Order_courierName.contains("Fedex") || Order_courierName.contains("FEDEX-SURFACE")) {
                                            IMAGE_RESOURCE = R.drawable.fedex;
                                        }
                                        else if(Order_courierName.contains("Bluedart")) {
                                            IMAGE_RESOURCE = R.drawable.bluedart;
                                        }
                                        else if(Order_courierName.contains("Dotzot")) {
                                            IMAGE_RESOURCE = R.drawable.dotzot;
                                        }
                                        else if(Order_courierName.contains("Xpressbees")) {
                                            IMAGE_RESOURCE = R.drawable.xpressbees;
                                        }
                                        else if(Order_courierName.contains("Delhivery")) {
                                            IMAGE_RESOURCE = R.drawable.delhivery;
                                        }
                                        else if(Order_courierName.contains("DHL")) {
                                            IMAGE_RESOURCE = R.drawable.dhl;
                                        }
                                        else if(Order_courierName.contains("Ekart")) {
                                            IMAGE_RESOURCE = R.drawable.ekart;
                                        }
                                        else {
                                            IMAGE_RESOURCE = R.drawable.no_image;
                                        }

                                        courierImage.setImageResource(IMAGE_RESOURCE);

                                        courierCompanyPrice.setText(Order_courierPrice + " (Packaging and Insurance included if selected)");

                                        if(Order_Payment_Mode.equalsIgnoreCase("ONLINE")) {
                                            paymentMode.setTextColor(Color.parseColor("#90EE90"));
                                        }
                                        else {
                                            paymentMode.setTextColor(Color.parseColor("#800000"));
                                        }
                                        paymentMode.setText(Order_Payment_Mode);

                                        progress.dismiss();
                                        super.onPostExecute(o);
                                    }
                                }.execute();
                            }
                        });
                    } else {
                        message = "Some Error";
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            progressDialog.dismiss();
            ConstraintLayout constraintLayout = findViewById(R.id.confirmView);
            Snackbar.make(constraintLayout, message, Snackbar.LENGTH_LONG);
            super.onPostExecute(o);
        }
    }

}
