package akshay.tech.com.shipdots;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.SignInButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import akshay.tech.com.shipdots.OtherTools.InternetConnection;
import akshay.tech.com.shipdots.OtherTools.Orders;
import akshay.tech.com.shipdots.OtherTools.Property;
import akshay.tech.com.shipdots.OtherTools.RecentOrderAdapter;

import static akshay.tech.com.shipdots.MainActivity.final_id;
import static akshay.tech.com.shipdots.OtherTools.Orders.orderProperties;
import static akshay.tech.com.shipdots.SplashScreen.conn;
import static akshay.tech.com.shipdots.SplashScreen.db_password;
import static akshay.tech.com.shipdots.SplashScreen.db_username;
import static akshay.tech.com.shipdots.SplashScreen.stmt;
import static akshay.tech.com.shipdots.SplashScreen.url;


public class LoginScreen extends AppCompatActivity {

    //public static String url = "jdbc:mysql://app.vourier.com/logistics_v2?useSSL=false";
            //+"?verifyServerCertificate=true"+
            //"&useSSL=true"+
           // "&requireSSL=true";
    //public static String db_username = "admin";
   // public static String db_password = "Ankur123@";
    private TextView goToRegister;
    private EditText username, password;
    String Email, Password = "";
    File password_file = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/pw.txt");
    int counter = 0;
    List<AuthUI.IdpConfig> providers;
    private static final int My_Request_Code = 102;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);


      /*  //Login Providers
        providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build()

        );
        showSignIn();

       */

        askForPermissions();

        Button loginButton = findViewById(R.id.button);
        SignInButton gmail = findViewById(R.id.sign_in_button);

        username = findViewById(R.id.editText);
        password = findViewById(R.id.editText2);

        goToRegister = findViewById(R.id.goToRegisterActivity);

        //CREATE PASSWORD FILE IF IT ISN'T THERE
        File folder = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/");
        if(!folder.exists()) {
            try {
                folder.mkdirs();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        //IF PASSWORD SAVED
        String[] details = checkForPasswordFile();
        if(details != null) {
            Email = details[0];
            Password = details[1];

            new LoginTask().execute();
           // new get10RecentOrders().execute();
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vibrate();

                Email = username.getText().toString();
                Password = password.getText().toString();

                if(InternetConnection.checkConnection(getApplicationContext())) {
                    new LoginTask().execute();
                }
                else {
                    new AlertDialog.Builder(new ContextThemeWrapper(LoginScreen.this, R.style.myDialog)).setTitle("Connection Error")
                            .setMessage("Failed to establish a network connection. Please enable Wi-Fi or Data Connection to continue.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finishAffinity();
                                }
                            })
                            .show();
                }
            }
        });



        goToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginScreen.this, RegisterActivity.class));
            }
        });
    }

    private void askForPermissions() {
        try {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},101);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String[] checkForPasswordFile() {
        File passwordFile = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/pw.txt");
        String[] det_array = null;
        if(passwordFile.exists()) {
            String line;
            String details = "";
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(passwordFile)));
                while ((line = reader.readLine())!=null) {
                    details = line;
                }
                det_array = details.split(" ");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return det_array;
    }

    public void vibrate() {
        Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(100);
        }
    }

   /* private void showSignIn(){

        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),My_Request_Code
        );
    }

    */

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == My_Request_Code)
        {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if(resultCode == RESULT_OK)
            {
                //Getting User
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                Toast.makeText(this,""+user.getEmail(),Toast.LENGTH_SHORT).show();

            }
            else {
                Toast.makeText(this,""+response.getError().getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    }

    */

    @SuppressLint("StaticFieldLeak")
    class LoginTask extends AsyncTask {

        private ProgressDialog p;

        @Override
        protected void onPreExecute() {
            p = new ProgressDialog(LoginScreen.this);
            p.setMessage("Logging in..");
            p.setIndeterminate(false);
            p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p.setCancelable(false);
            p.show();
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            //StrictMode.setThreadPolicy(policy);
            try {
                //Class.forName("com.mysql.jdbc.Driver");
                //Properties props = new Properties();
                //props.setProperty("user", LoginScreen.db_username);
                //props.setProperty("password", LoginScreen.db_password);
                //props.setProperty("useSSL","true");
                //Connection conn = DriverManager.getConnection(LoginScreen.url, LoginScreen.db_username,LoginScreen.db_password);
                //Statement stmt = conn.createStatement();


                    /*if(conn.isClosed()) {
                        conn = DriverManager.getConnection(url, db_username, db_password);
                        //stmt = conn.createStatement();
                    }

                     */
                ResultSet rs = stmt.executeQuery("SELECT * FROM customer_details WHERE Email = '"+Email+"' AND password ='"+Password+"'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        counter++;
                    }
                }
                else {
                    if(password_file.exists()) {
                        ConstraintLayout constraintLayout = findViewById(R.id.constraint_layout_login_screen);
                        if(password_file.delete()) {
                            Snackbar.make(constraintLayout, "ID or Password Changed!", Snackbar.LENGTH_LONG);
                        }
                    }
                }
                //conn.close();

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            p.dismiss();
            if(counter > 0) {

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                ArrayList<String> addressDetails = new ArrayList<>();

                try {

                    if(conn!=null){
                        if(conn.isClosed()) {
                            conn = DriverManager.getConnection(url, db_username, db_password);
                            //stmt = conn.createStatement();
                        }
                    addressDetails.add("Select Pickup Address*");
                    //Class.forName("com.mysql.jdbc.Driver");
                    //Connection conn = DriverManager.getConnection(url, db_username, db_password);
                    //Statement stmt = conn.createStatement();

                    ResultSet rs = stmt.executeQuery("SELECT * FROM customer_address_mobile WHERE Address_Email = '"+Email+"' ");
                    if(rs.next()) {
                        rs.beforeFirst();
                        while(rs.next()) {
                            addressDetails.add( rs.getString("Address_Alias"));
                        }
                    }
                    //conn.close();
                }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(LoginScreen.this, MainActivity.class);
                intent.putExtra("ID", Email);
                intent.putExtra("PASSWORD", Password);
                intent.putExtra("ADDRESS_LIST", addressDetails);

                startActivity(intent);
            }
            super.onPostExecute(o);
        }
    }

    @Override
    public void onBackPressed() {
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog)).setTitle("Exit").setMessage("Do you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    })
                    .show();
    }


  /* @SuppressLint("StaticFieldLeak")
    public class get10RecentOrders extends AsyncTask {

        int IMAGE_RESOURCE;
        String OrderID, courierName, awbNumber, paymentMethod, price, status, receiver_name;


        @Override
        protected Object doInBackground(Object[] objects) {

            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            //StrictMode.setThreadPolicy(policy);

            try {
                //Class.forName("com.mysql.jdbc.Driver");
               // Connection conn = DriverManager.getConnection(LoginScreen.url, LoginScreen.db_username, LoginScreen.db_password);
                //Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM customer_orders WHERE Email ='"+final_id+"' ORDER BY timestamp DESC LIMIT 10 ");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        OrderID = rs.getString("Order_ID");
                        courierName = rs.getString("operator");
                        awbNumber = rs.getString("AWB_Number");
                        paymentMethod = rs.getString("Payment_Mode");
                        price = "₹"+rs.getString("Price_Paid_By_Customer");
                        status = rs.getString("status");
                        receiver_name = rs.getString("Customer_Name");

                        if(courierName.isEmpty())
                            courierName = "NOT ASSIGNED";

                        if(awbNumber.isEmpty()) {
                            awbNumber = "Not Assigned";
                        }

                        if(paymentMethod.isEmpty()) {
                            paymentMethod = rs.getString("COD");
                            if(paymentMethod.equalsIgnoreCase("Yes"))
                                paymentMethod = "POSTPAID";
                            else
                                paymentMethod = "PREPAID";
                        }

                        if(courierName.contains("Fedex") || courierName.contains("FEDEX-SURFACE")) {
                            IMAGE_RESOURCE = R.drawable.fedex;
                        }
                        else if(courierName.contains("Bluedart")) {
                            IMAGE_RESOURCE = R.drawable.bluedart;
                        }
                        else if(courierName.contains("Dotzot")) {
                            IMAGE_RESOURCE = R.drawable.dotzot;
                        }
                        else if(courierName.contains("Xpressbees")) {
                            IMAGE_RESOURCE = R.drawable.xpressbees;
                        }
                        else if(courierName.contains("Delhivery")) {
                            IMAGE_RESOURCE = R.drawable.delhivery;
                        }
                        else if(courierName.contains("DHL")) {
                            IMAGE_RESOURCE = R.drawable.dhl;
                        }
                        else if(courierName.contains("Ekart")) {
                            IMAGE_RESOURCE = R.drawable.ekart;
                        }
                        else {
                            IMAGE_RESOURCE = R.drawable.box;
                        }

                        orderProperties.add(new Property(IMAGE_RESOURCE, OrderID, courierName, awbNumber, paymentMethod, price, status, receiver_name));
                    }
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginScreen.this, "No orders!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                //conn.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            final ArrayAdapter<Property> adapter = new RecentOrderAdapter(LoginScreen.this, 0, orderProperties);
            Orders.listView.setAdapter(adapter);


            Orders.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Property property = orderProperties.get(i);

                    Log.e("ORDER-ID_Login", property.getOrderID());
                }
            });
            super.onPostExecute(o);
        }
    }

   */


}

