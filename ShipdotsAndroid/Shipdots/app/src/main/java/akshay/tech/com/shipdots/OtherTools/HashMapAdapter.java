package akshay.tech.com.shipdots.OtherTools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import akshay.tech.com.shipdots.R;

public class HashMapAdapter extends BaseAdapter {
    private final ArrayList mData;

    HashMapAdapter(Map<String, String> map) {
        mData = new ArrayList();
        mData.addAll(map.entrySet());
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, String> getItem(int position) {
        return (Map.Entry) mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO implement you own logic with ID
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.courier_item, parent, false);
        } else {
            result = convertView;
        }

        Map.Entry<String, String> item = getItem(position);

        // TODO replace findViewById by ViewHolder
        int IMAGE_RESOURCE = 0;
        if(item.getKey().contains("Fedex") || item.getKey().contains("FEDEX-SURFACE")) {
            IMAGE_RESOURCE = R.drawable.fedex;
        }
        else if(item.getKey().contains("Bluedart")) {
            IMAGE_RESOURCE = R.drawable.bluedart;
        }
        else if(item.getKey().contains("Dotzot")) {
            IMAGE_RESOURCE = R.drawable.dotzot;
        }
        else if(item.getKey().contains("Xpressbees")) {
            IMAGE_RESOURCE = R.drawable.xpressbees;
        }
        else if(item.getKey().contains("Delhivery")) {
            IMAGE_RESOURCE = R.drawable.delhivery;
        }
        else if(item.getKey().contains("DHL")) {
            IMAGE_RESOURCE = R.drawable.dhl;
        }
        else if(item.getKey().contains("Ekart")) {
            IMAGE_RESOURCE = R.drawable.ekart;
        }
        else {
            IMAGE_RESOURCE = R.drawable.no_image;
        }
        ((ImageView) result.findViewById(R.id.courierImage)).setImageResource(IMAGE_RESOURCE);
        if(item.getKey().contains("Fedex") || item.getKey().contains("FEDEX-SURFACE") || item.getKey().contains("Bluedart") || item.getKey().contains("Dotzot") || item.getKey().contains("Xpressbees") || item.getKey().contains("Delhivery")) {
            ((TextView) result.findViewById(android.R.id.text1)).setText(item.getKey());
            String[] price = item.getValue().split("@");
            ((TextView) result.findViewById(android.R.id.text2)).setText(price[0]);
        }
        return result;
    }
}
