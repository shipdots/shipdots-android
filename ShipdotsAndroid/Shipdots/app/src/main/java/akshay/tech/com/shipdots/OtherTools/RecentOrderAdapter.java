package akshay.tech.com.shipdots.OtherTools;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import akshay.tech.com.shipdots.R;

public class RecentOrderAdapter extends ArrayAdapter<Property> {

    private Context context;
    private List<Property> orderProperties;

    public RecentOrderAdapter(@NonNull Context context, int resource, ArrayList<Property> objects) {
        super(context, resource, objects);

        this.context = context;
        this.orderProperties = objects;
    }

    //called when rendering the list
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        //get the property we are displaying
        final Property property = orderProperties.get(position);

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        @SuppressLint({"ViewHolder", "InflateParams"}) View view = inflater.inflate(R.layout.orders_list_adapter, null);

        final TextView orderID = view.findViewById(android.R.id.text1);
        TextView receiver_name = view.findViewById(R.id.receiverName);
        final TextView courier_name = view.findViewById(R.id.courier_name);
        TextView payment_method = view.findViewById(R.id.payment);
        TextView status = view.findViewById(R.id.status);
        final TextView awbNumber = view.findViewById(R.id.awb_number);
        TextView total_price = view.findViewById(R.id.customer_paid);
        ImageView courierPhoto = view.findViewById(R.id.courierPhoto);
        Button button = view.findViewById(R.id.completeTransaction);
        Button button1 = view.findViewById(R.id.trackOrder);

        orderID.setText(property.getOrderID());
        receiver_name.setText(property.getReceiverName());
        courier_name.setText(property.getcourierName());
        payment_method.setText(property.getpaymentMethod());
        status.setText(property.getstatus());
        awbNumber.setText(property.getawbNumber());
        total_price.setText(property.getPrice());
        courierPhoto.setImageResource(property.getIMAGE_RESOURCE());

        courier_name.setVisibility(View.GONE);

        if(!awbNumber.getText().toString().equalsIgnoreCase("Not Assigned")) {
            awbNumber.setTextColor(Color.parseColor("#285FDB"));
        }
        if(total_price.getText().toString().length()>1) {
            if(total_price.getText().toString().charAt(1)=='0') {
                button.setVisibility(View.VISIBLE);
                button1.setVisibility(View.GONE);
            }
            else {
                button.setVisibility(View.GONE);
                button1.setVisibility(View.VISIBLE);
            }
        }
        else {
            button.setVisibility(View.VISIBLE);
            button1.setVisibility(View.GONE);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, CompletePreviousOrder.class).putExtra("ORDER-ID", orderID.getText().toString()));
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!awbNumber.getText().toString().equals("Not Assigned")) {
                    String courierName = courier_name.getText().toString();
                    if(courierName.contains("Fedex") || courierName.contains("FEDEX-SURFACE")) {
                        context.startActivity(new Intent(context, TrackWebView.class).putExtra("AWB NUMBER", awbNumber.getText().toString()).putExtra("COURIER NAME", "Fedex"));
                    }
                    else if(courierName.contains("Xpressbees")) {
                        context.startActivity(new Intent(context, TrackWebView.class).putExtra("AWB NUMBER", awbNumber.getText().toString()).putExtra("COURIER NAME", "Xpressbees"));
                    }
                    else if(courierName.contains("Dotzot")) {
                        context.startActivity(new Intent(context, TrackWebView.class).putExtra("AWB NUMBER", awbNumber.getText().toString()).putExtra("COURIER NAME", "Dotzot"));
                    }
                    else if(courierName.contains("Delhivery")) {
                        context.startActivity(new Intent(context, TrackWebView.class).putExtra("AWB NUMBER", awbNumber.getText().toString()).putExtra("COURIER NAME", "Delhivery"));
                    }
                    else if(courierName.contains("Bluedart")) {
                        context.startActivity(new Intent(context, TrackWebView.class).putExtra("AWB NUMBER", awbNumber.getText().toString()).putExtra("COURIER NAME", "Bluedart"));
                    }
                }
            }
        });

        return view;
    }
}
