package akshay.tech.com.shipdots.OtherTools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import akshay.tech.com.shipdots.MainActivity;
import akshay.tech.com.shipdots.R;

public class Chat extends AppCompatActivity {

    private RelativeLayout pickupSupport, deliverySupport, billingSupport, technicalSupport, feedbackSupport;
    private String EMAIL = "pritroniks@gmail.com";
    android.app.AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Conversations");

        pickupSupport = findViewById(R.id.pickupSupport);
        deliverySupport = findViewById(R.id.deliverySupport);
        billingSupport = findViewById(R.id.billingSupport);
        technicalSupport = findViewById(R.id.technicalSupport);
        feedbackSupport = findViewById(R.id.feedbackSupport);


        pickupSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                LayoutInflater layoutInflater = LayoutInflater.from(Chat.this);
                @SuppressLint("InflateParams") final View changePasswordView = layoutInflater.inflate(R.layout.chats_layouts, null);

                final EditText awb_numbers = changePasswordView.findViewById(R.id.awb_numbers);
                final Button sendMail = changePasswordView.findViewById(R.id.sendMail);
                final Button cancelMail = changePasswordView.findViewById(R.id.cancelMail);
                final TextView chatType = changePasswordView.findViewById(R.id.chatType);

                chatType.setText("PICKUP SUPPORT");

                sendMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(!awb_numbers.getText().toString().isEmpty()) {
                            Intent mailIntent = new Intent(Intent.ACTION_SEND);
                            mailIntent.setType("text/plain");
                            mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL});
                            mailIntent.putExtra(Intent.EXTRA_SUBJECT, "PICKUP SUPPORT FOR "+awb_numbers.getText().toString());
                            startActivity(Intent.createChooser(mailIntent, "Pick an Email Provider"));
                        }
                        else {
                            Toast.makeText(Chat.this, "Please enter AWB Number of the order", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                cancelMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(alertDialog != null) {
                            alertDialog.dismiss();
                            alertDialog = null;
                        }
                    }
                });

                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Chat.this);
                alertDialogBuilder.setView(changePasswordView);

                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        deliverySupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                LayoutInflater layoutInflater = LayoutInflater.from(Chat.this);
                @SuppressLint("InflateParams") final View changePasswordView = layoutInflater.inflate(R.layout.chats_layouts, null);

                final EditText awb_numbers = changePasswordView.findViewById(R.id.awb_numbers);
                final Button sendMail = changePasswordView.findViewById(R.id.sendMail);
                final Button cancelMail = changePasswordView.findViewById(R.id.cancelMail);
                final TextView chatType = changePasswordView.findViewById(R.id.chatType);

                chatType.setText("DELIVERY SUPPORT");

                sendMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(!awb_numbers.getText().toString().isEmpty()) {
                            Intent mailIntent = new Intent(Intent.ACTION_SEND);
                            mailIntent.setType("text/plain");
                            mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL});
                            mailIntent.putExtra(Intent.EXTRA_SUBJECT, "DELIVERY SUPPORT FOR "+awb_numbers.getText().toString());
                            startActivity(Intent.createChooser(mailIntent, "Pick an Email Provider"));
                        }
                        else {
                            Toast.makeText(Chat.this, "Please enter AWB Number of the order", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                cancelMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(alertDialog != null) {
                            alertDialog.dismiss();
                            alertDialog = null;
                        }
                    }
                });

                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Chat.this);
                alertDialogBuilder.setView(changePasswordView);

                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        billingSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                LayoutInflater layoutInflater = LayoutInflater.from(Chat.this);
                @SuppressLint("InflateParams") final View chatView = layoutInflater.inflate(R.layout.chats_layouts, null);

                final EditText awb_numbers = chatView.findViewById(R.id.awb_numbers);
                final Button sendMail = chatView.findViewById(R.id.sendMail);
                final Button cancelMail = chatView.findViewById(R.id.cancelMail);
                final TextView chatType = chatView.findViewById(R.id.chatType);

                chatType.setText("BILLING SUPPORT");

                sendMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(!awb_numbers.getText().toString().isEmpty()) {
                            Intent mailIntent = new Intent(Intent.ACTION_SEND);
                            mailIntent.setType("text/plain");
                            mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL});
                            mailIntent.putExtra(Intent.EXTRA_SUBJECT, "BILLING SUPPORT FOR "+awb_numbers.getText().toString());
                            startActivity(Intent.createChooser(mailIntent, "Pick an Email Provider"));
                        }
                        else {
                            Toast.makeText(Chat.this, "Please enter AWB Number of the order", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                cancelMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(alertDialog != null) {
                            alertDialog.dismiss();
                            alertDialog = null;
                        }
                    }
                });

                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Chat.this);
                alertDialogBuilder.setView(chatView);

                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        technicalSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                LayoutInflater layoutInflater = LayoutInflater.from(Chat.this);
                @SuppressLint("InflateParams") final View changePasswordView = layoutInflater.inflate(R.layout.chats_layouts, null);

                final EditText awb_numbers = changePasswordView.findViewById(R.id.awb_numbers);
                final Button sendMail = changePasswordView.findViewById(R.id.sendMail);
                final Button cancelMail = changePasswordView.findViewById(R.id.cancelMail);
                final TextView chatType = changePasswordView.findViewById(R.id.chatType);

                chatType.setText("TECHNICAL SUPPORT");

                sendMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(!awb_numbers.getText().toString().isEmpty()) {
                            Intent mailIntent = new Intent(Intent.ACTION_SEND);
                            mailIntent.setType("text/plain");
                            mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL});
                            mailIntent.putExtra(Intent.EXTRA_SUBJECT, "TECHNICAL SUPPORT FOR "+awb_numbers.getText().toString());
                            startActivity(Intent.createChooser(mailIntent, "Pick an Email Provider"));
                        }
                        else {
                            Toast.makeText(Chat.this, "Please enter AWB Number of the order", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                cancelMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(alertDialog != null) {
                            alertDialog.dismiss();
                            alertDialog = null;
                        }
                    }
                });

                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Chat.this);
                alertDialogBuilder.setView(changePasswordView);

                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        feedbackSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                LayoutInflater layoutInflater = LayoutInflater.from(Chat.this);
                @SuppressLint("InflateParams") final View changePasswordView = layoutInflater.inflate(R.layout.chats_layouts, null);

                final EditText awb_numbers = changePasswordView.findViewById(R.id.awb_numbers);
                final Button sendMail = changePasswordView.findViewById(R.id.sendMail);
                final Button cancelMail = changePasswordView.findViewById(R.id.cancelMail);
                final TextView chatType = changePasswordView.findViewById(R.id.chatType);

                chatType.setText("FEEDBACK");

                sendMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(!awb_numbers.getText().toString().isEmpty()) {
                            Intent mailIntent = new Intent(Intent.ACTION_SEND);
                            mailIntent.setType("text/plain");
                            mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {EMAIL});
                            mailIntent.putExtra(Intent.EXTRA_SUBJECT, "FEEDBACK FOR "+awb_numbers.getText().toString());
                            startActivity(Intent.createChooser(mailIntent, "Pick an Email Provider"));
                        }
                        else {
                            Toast.makeText(Chat.this, "Please enter AWB Number of the order", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                cancelMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vibrate();
                        if(alertDialog != null) {
                            alertDialog.dismiss();
                            alertDialog = null;
                        }
                    }
                });

                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Chat.this);
                alertDialogBuilder.setView(changePasswordView);

                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    public void vibrate() {
        Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(100);
        }
    }
}
