package akshay.tech.com.shipdots.OtherTools;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import akshay.tech.com.shipdots.LoginScreen;
import akshay.tech.com.shipdots.MainActivity;
import akshay.tech.com.shipdots.R;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static akshay.tech.com.shipdots.MainActivity.final_id;
import static akshay.tech.com.shipdots.SplashScreen.conn;
import static akshay.tech.com.shipdots.SplashScreen.db_password;
import static akshay.tech.com.shipdots.SplashScreen.db_username;
import static akshay.tech.com.shipdots.SplashScreen.stmt;
import static akshay.tech.com.shipdots.SplashScreen.url;

public class Profile extends AppCompatActivity {

    public static CircleImageView userImage;
    private TextView username;
    private EditText userID, userPassword;
    private TextView totalOrders, pendingOrders, IntransitOrders, cancelledOrders;
    private String ImageLink, Name, checkPass;
    android.app.AlertDialog alertDialog;
    private Uri mCropImageUri;
    ImageButton changeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("User Profile");

        ImageLink = getIntent().getStringExtra("IMAGE_LINK");
        Name = getIntent().getStringExtra("NAME");

        userImage = findViewById(R.id.image);
        username = findViewById(R.id.name);
        userID = findViewById(R.id.registeredID);
        userPassword = findViewById(R.id.passsword);
        totalOrders = findViewById(R.id.totalOrders);
        pendingOrders = findViewById(R.id.pendingOrders);
        IntransitOrders = findViewById(R.id.intransitOrders);
        cancelledOrders = findViewById(R.id.cancelledOrders);
        changeImage = findViewById(R.id.changeDP);

        userID.setKeyListener(null);
        userPassword.setKeyListener(null);

        userPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });

        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("|","image change");
                vibrate();
                new AlertDialog.Builder(new ContextThemeWrapper(Profile.this, R.style.myDialog)).setTitle("Select an option").setMessage("What do you want to do?")
                        .setPositiveButton("View Image", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent showImage = new Intent(Profile.this, ViewImage.class);
                                showImage.putExtra("URL", ImageLink);
                                startActivity(showImage);
                            }
                        })
                        .setNegativeButton("Change Image", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                changeImageNow();
                            }
                        })
                        .show();
            }
        });

        new fillProfile().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    public class fillProfile extends AsyncTask {

        ProgressDialog progressDialog;
        int total, pending, intransit, cancelled;
        String passwordDet, imgURL;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(Profile.this);
            progressDialog.setMessage("Syncing..");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(getBaseContext()).load(MainActivity.ImageLink).networkPolicy(NetworkPolicy.NO_CACHE).into(userImage);
                }
            });
            username.setText(Name);

            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            //StrictMode.setThreadPolicy(policy);
            try {
               // Class.forName("com.mysql.jdbc.Driver");
                // Connection conn = DriverManager.getConnection(LoginScreen.url, LoginScreen.db_username, LoginScreen.db_password);

                if(conn.isClosed()) {
                    conn = DriverManager.getConnection(url, db_username, db_password);
                    //stmt = conn.createStatement();
                }
               Statement slow = conn.createStatement();
               ResultSet rs = slow.executeQuery("SELECT * FROM customer_orders WHERE Email = '"+final_id+"'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        rs.last();
                        total = rs.getRow();
                    }
                }
                else {
                    total = 0;
                }

                rs = slow.executeQuery("SELECT * FROM customer_orders WHERE Email = '"+final_id+"' AND Status = 'PROCESSING'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        rs.last();
                        pending = rs.getRow();
                    }
                }
                else {
                    pending = 0;
                }

                rs = slow.executeQuery("SELECT * FROM customer_orders WHERE Email = '"+final_id+"' AND Status = 'CANCELLED'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        rs.last();
                        cancelled = rs.getRow();
                    }
                }
                else {
                    cancelled = 0;
                }

                rs = slow.executeQuery("SELECT Password FROM customer_details WHERE Email = '"+final_id+"'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        passwordDet = rs.getString("Password");
                    }
                }
                else {
                    passwordDet = "";
                }

                rs = slow.executeQuery("SELECT ImageLink FROM customer_details WHERE Email = '"+final_id+"'");
                if(rs.next()) {
                    rs.beforeFirst();
                    while(rs.next()) {
                        imgURL = rs.getString("ImageLink");
                    }
                }
                else {
                    imgURL = "";
                }

               // conn.close();
            }

            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            totalOrders.setText(String.valueOf(total));
            pendingOrders.setText(String.valueOf(pending));
            cancelledOrders.setText(String.valueOf(cancelled));
            intransit = total - (pending+cancelled);
            IntransitOrders.setText(String.valueOf(intransit));
            userID.setText(final_id);
            progressDialog.dismiss();
            userPassword.setText(passwordDet);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(getBaseContext()).load(imgURL).networkPolicy(NetworkPolicy.NO_CACHE).into(userImage);
                }
            });

            checkPass = passwordDet;
            super.onPostExecute(o);
        }
    }

    public void changePassword() {
        LayoutInflater layoutInflater = LayoutInflater.from(Profile.this);
        @SuppressLint("InflateParams") final View changePasswordView = layoutInflater.inflate(R.layout.change_password, null);

        final EditText oldPassword = changePasswordView.findViewById(R.id.oldPassword);
        final EditText newPassword = changePasswordView.findViewById(R.id.newPassword);
        final Button applyButton = changePasswordView.findViewById(R.id.change);
        final Button cancelButton = changePasswordView.findViewById(R.id.cancel);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                if(!oldPassword.getText().toString().isEmpty() && !newPassword.getText().toString().isEmpty()) {
                    if(oldPassword.getText().toString().equals(checkPass)) {
                        runOnUiThread(new Runnable() {
                            @SuppressLint("StaticFieldLeak")
                            @Override
                            public void run() {
                                new AsyncTask() {

                                    ProgressDialog changePass;
                                    String message;

                                    @Override
                                    protected void onPreExecute() {
                                        changePass = new ProgressDialog(Profile.this);
                                        changePass = new ProgressDialog(Profile.this);
                                        changePass.setMessage("Changing Password..");
                                        changePass.setIndeterminate(false);
                                        changePass.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                        changePass.setCancelable(false);
                                        changePass.show();
                                        super.onPreExecute();
                                    }

                                    @Override
                                    protected Object doInBackground(Object[] objects) {

                             //          StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                        //StrictMode.setThreadPolicy(policy);
                                        try {
                                         //   Class.forName("com.mysql.jdbc.Driver");
                                           // Connection conn = DriverManager.getConnection(LoginScreen.url, LoginScreen.db_username, LoginScreen.db_password);
                                            //Statement stmt = conn.createStatement();



                                            if(conn.isClosed()) {
                                                conn = DriverManager.getConnection(url, db_username, db_password);
                                                //stmt = conn.createStatement()
                                            }
                                            Statement sew = conn.createStatement();
                                            sew.executeUpdate("UPDATE customer_details SET Password = '"+newPassword.getText().toString()+"' WHERE Email = '"+final_id+"'");
                                            //conn.close();
                                            message = "Password changed successfully";
                                        }
                                        catch (Exception e) {
                                            e.printStackTrace();
                                            message = "Some error occurred";
                                        }
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Object o) {
                                        changePass.dismiss();
                                        ConstraintLayout constraintLayout = findViewById(R.id.profileLayout);
                                        Snackbar.make(constraintLayout, message, Snackbar.LENGTH_LONG).show();

                                        if(alertDialog != null) {
                                            alertDialog.dismiss();
                                            alertDialog = null;
                                        }
                                        super.onPostExecute(o);
                                    }
                                }.execute();
                            }
                        });
                    }
                    else {
                        Toast.makeText(Profile.this, "Old Password is not correct", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(Profile.this, "Both fields are required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrate();
                if(alertDialog != null) {
                    alertDialog.dismiss();
                    alertDialog = null;
                }
            }
        });

        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Profile.this);
        alertDialogBuilder.setView(changePasswordView);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void vibrate() {
        Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(100);
        }
    }

    public void changeImageNow() {
        startActivityForResult(CropImage.getPickImageChooserIntent(getBaseContext()), CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getBaseContext(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getBaseContext(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                userImage.setImageURI(result.getUri());
                MainActivity.userImage.setImageURI(result.getUri());
                Bitmap photo = ((BitmapDrawable)userImage.getDrawable()).getBitmap();
                new uploadImage().execute(photo);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getBaseContext(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(getBaseContext(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    @SuppressLint("StaticFieldLeak")
    public class uploadImage extends AsyncTask<Bitmap, Void, Void> {

        private ProgressDialog p;
        String message = "";

        @Override
        protected void onPreExecute() {
            p = new ProgressDialog(Profile.this);
            p.setMessage("Updating Image..");
            p.setIndeterminate(false);
            p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p.setCancelable(false);
            p.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Bitmap... bitmaps) {

            Bitmap image = bitmaps[0];

            String imageName = final_id+".jpg";
            OutputStream os = null;
            try {
                os = new BufferedOutputStream(new FileOutputStream(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/"+imageName));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            image.compress(Bitmap.CompressFormat.JPEG, 60, os);

            File file = new File(Environment.getExternalStorageDirectory()+"/Android/data/Shipdots/"+imageName);

            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("submit_image", file.getName(), RequestBody.create(MediaType.parse("image/jpeg"), file)).build();
            Request request = new Request.Builder().url("https://app.vourier.com/upload_image.php").post(requestBody).build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if(response.body() != null) {
                    String uploadResponse = response.body().string();
                    Log.e("RESPONSE", uploadResponse);
                    if (uploadResponse.contains("done")) {
                        message = "Profile picture changed";
                        MainActivity.ImageLink = uploadResponse.split(" ")[1];
                        deleteCache(Profile.this);
                    }
                    else {
                        message = "Upload failed";
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            p.dismiss();
            final ConstraintLayout constraintLayout = findViewById(R.id.profileLayout);
            Snackbar.make(constraintLayout, message, Snackbar.LENGTH_LONG).show();
            super.onPostExecute(aVoid);
        }

        void deleteCache(Context context) {
            try {
                File dir = context.getCacheDir();
                deleteDir(dir);
            } catch (Exception e) { e.printStackTrace();}
        }

        boolean deleteDir(File dir) {
            if (dir != null && dir.isDirectory()) {
                String[] children = dir.list();
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
                return dir.delete();
            } else if(dir!= null && dir.isFile()) {
                return dir.delete();
            } else {
                return false;
            }
        }
    }
}
