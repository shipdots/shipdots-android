package akshay.tech.com.shipdots;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;

import static akshay.tech.com.shipdots.SplashScreen.conn;
import static akshay.tech.com.shipdots.SplashScreen.db_password;
import static akshay.tech.com.shipdots.SplashScreen.db_username;
import static akshay.tech.com.shipdots.SplashScreen.stmt;
import static akshay.tech.com.shipdots.SplashScreen.url;

public class RegisterActivity extends AppCompatActivity {

        private EditText phoneNumber, password, first_name, last_name;
        private Button registerButton, loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Register Account");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        phoneNumber = findViewById(R.id.mobileNumber);
        password = findViewById(R.id.password);
        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);
        registerButton = findViewById(R.id.register);
        loginButton = findViewById(R.id.loginButton);

        loginButton.setVisibility(View.GONE);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(phoneNumber.getText().toString().isEmpty() || password.getText().toString().isEmpty() || first_name.getText().toString().isEmpty() || last_name.getText().toString().isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "All Fields are required!", Toast.LENGTH_SHORT).show();
                }
                else {
                    if(checkForRegistration()) {
                        Toast.makeText(RegisterActivity.this, "Phone Number Already Registered!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        try {
                           // Class.forName("com.mysql.jdbc.Driver");
                            //Connection conn = DriverManager.getConnection(LoginScreen.url, LoginScreen.db_username, LoginScreen.db_password);
                            //Statement stmt = conn.createStatement();

                            if(conn.isClosed()){
                                conn = DriverManager.getConnection(url,db_username,db_password);
                                stmt = conn.createStatement();
                            }
                            stmt.executeUpdate("INSERT INTO customer_details(Email, RegistrationType, Password, FirstName, LastName)" + "VALUES('"+phoneNumber.getText().toString()+"', 'MOBILE', '"+password.getText().toString()+"', '"+first_name.getText().toString()+"', '"+last_name.getText().toString()+"')");
                            Toast.makeText(RegisterActivity.this, "Account Created.", Toast.LENGTH_SHORT).show();

                            registerButton.setEnabled(false);
                            registerButton.setClickable(false);
                            registerButton.setAlpha(.5f);
                            loginButton.setVisibility(View.VISIBLE);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToMainPage = new Intent(RegisterActivity.this, MainActivity.class);
                goToMainPage.putExtra("ID", phoneNumber.getText().toString());
                goToMainPage.putExtra("PASSWORD", password.getText().toString());
                startActivity(goToMainPage);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), LoginScreen.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    public boolean checkForRegistration() {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            //Connection conn = DriverManager.getConnection(LoginScreen.url, LoginScreen.db_username, LoginScreen.db_password);
            //Statement stmt = conn.createStatement();

            if(conn.isClosed()){
                conn = DriverManager.getConnection(url,db_username,db_password);
                stmt = conn.createStatement();
            }
            ResultSet rs = stmt.executeQuery("SELECT * from customer_details where Email = '"+phoneNumber.getText().toString()+"'");
            return rs.next();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
