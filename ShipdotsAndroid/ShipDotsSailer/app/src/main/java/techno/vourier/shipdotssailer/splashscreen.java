package techno.vourier.shipdotssailer;


import android.annotation.SuppressLint;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import techno.vourier.shipdotssailer.R;

import static java.lang.Boolean.TRUE;


public class splashscreen extends AppCompatActivity {


    public static String url = "jdbc:mysql://app.vourier.com/logistics_v2?useSSL=false";
    public static String username = "admin";
    public static String password = "Ankur123@";
    public static Connection conor;
    public static Statement stmt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);



        new Handler().postDelayed(() -> {
            new connection().execute();

            finish();
        }, 500);


    }

    @SuppressLint("StaticFieldLeak")
    public class connection extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try {
                Class.forName("com.mysql.jdbc.Driver");
                conor = DriverManager.getConnection(url,username,password);
                stmt = conor.createStatement();



            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            Intent i = new Intent(splashscreen.this, Login.class);
            startActivity(i);
        }
    }


}
