package techno.vourier.shipdotssailer.Other_Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import techno.vourier.shipdotssailer.Login;
import techno.vourier.shipdotssailer.MainActivity;
import techno.vourier.shipdotssailer.R;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import techno.vourier.shipdotssailer.splashscreen;

import static techno.vourier.shipdotssailer.splashscreen.conor;
import static techno.vourier.shipdotssailer.splashscreen.password;
import static techno.vourier.shipdotssailer.splashscreen.stmt;
import static techno.vourier.shipdotssailer.splashscreen.url;
import static techno.vourier.shipdotssailer.splashscreen.username;


public class rider_day_summary extends AppCompatActivity {

    CircleImageView imageView;
    CardView cardView;
    TextView textView1,textView2,textView3,textView4,textView5,textView6,textView7,textView8,textView9;
    String rider_ID,price_paid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rider_day_summary);

        new fillprofile().execute();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Daily Summary");

        imageView = findViewById(R.id.s_imageView);
        cardView = findViewById(R.id.summary_cardView1);
        textView1 = findViewById(R.id.orderOverView);
        textView2 = findViewById(R.id.Totalorders);
        textView3 = findViewById(R.id.numberoforders);
        textView8 = findViewById(R.id.pickedorders);
        textView9 = findViewById(R.id.pickedno);
        textView4 = findViewById(R.id.cashcollect);
        textView5 = findViewById(R.id.cashCollected);
        textView6 = findViewById(R.id.failedorders);
        textView7 = findViewById(R.id.failedno);

        rider_ID = MainActivity.final_phone;
        price_paid = MainActivity.cash;



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }


    public class fillprofile extends AsyncTask {

        ProgressDialog pd;
        int total,pickedUp,failed;
        String cashcollected,dude;
        double count=0.0;
        public PreparedStatement statement1;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(rider_day_summary.this);
            pd.setMessage("Syncing");
            pd.setIndeterminate(false);
            pd.setCancelable(true);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

          //StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder().build();
           //StrictMode.setThreadPolicy(policy1);

            try {
              /*  Class.forName("com.mysql.jdbc.Driver");
                conor = DriverManager.getConnection(Login.url,Login.username,Login.password);
                stmt = conor.createStatement();

               */
                if(conor.isClosed()){
                        conor = DriverManager.getConnection(url,username,password);
                        stmt = conor.createStatement();
                }




                String dug = "SELECT * FROM customer_orders WHERE Rider_Number = '"+rider_ID+"' AND Status = 'PICKED UP' ";
                //String dug = "SELECT * FROM customer_orders WHERE RIDER_NUMBER =? AND Status =? ";
                ResultSet setting = stmt.executeQuery(dug);

                if(setting.next()){
                        setting.last();
                        pickedUp = setting.getRow();

                }else {
                    pickedUp = 0;
                }

                String stew = "SELECT * FROM customer_orders WHERE Rider_Number = '"+rider_ID+"'";
                ResultSet setting1 = stmt.executeQuery(stew);

                if(setting1.next()){
                    Log.e("NON ZERO", "TOTAL ORDERS");
                    setting1.last();
                    total = setting1.getRow();
                }else{
                    total = 0;
                }


                String soze = "SELECT * FROM customer_orders WHERE Rider_Number = '"+rider_ID+"' AND Status = 'FAILED' ";
                //String soze = "SELECT * FROM customer_orders WHERE RIDER_NUMBER = ? AND Status =? ";
                ResultSet setting2 = stmt.executeQuery(soze);
                if(setting2.next()){
                        setting2.last();
                        failed = setting2.getRow();

                }else{
                    failed = 0;
                }


                String forrest = "SELECT Price_Paid_By_Customer FROM customer_orders WHERE Rider_Number = '"+rider_ID+"' AND Payment_Mode = 'CASH' ";
                ResultSet setting3 = stmt.executeQuery(forrest);

                    if(setting3.next()){
                        setting3.beforeFirst();
                        while(setting3.next()) {
                            //setting.last();
                            cashcollected = setting3.getString("Price_Paid_By_Customer");
                            double result = Double.parseDouble(cashcollected);
                            count = count + result;
                        }

                    }
                    dude = String.valueOf(count);
                    Log.e("Total Value Collected",dude);



                    //conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            pd.dismiss();
            textView3.setText(String.valueOf(total));
            textView9.setText(String.valueOf(pickedUp));
            textView7.setText(String.valueOf(failed));
            textView5.setText(String.valueOf("Rs. "+dude));
            Log.e("Total Orders",textView3.getText().toString());
            Log.e("Failed Number of Orders",textView7.getText().toString());
            Log.e("Pickup Orders",textView9.getText().toString());
        }
    }


}
