package techno.vourier.shipdotssailer.Other_Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import techno.vourier.shipdotssailer.MainActivity;
import techno.vourier.shipdotssailer.R;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class rider_info extends AppCompatActivity {

    TextView tView1,tView2,tView3, eText1,eText2,eText3;

    CircleImageView r_ImageView;
    String UserImage;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rider_info);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("User Info");


        tView1 = findViewById(R.id.rider_textView1);
        tView2 = findViewById(R.id.rider_textView2);
        tView3 = findViewById(R.id.rider_textView3);
        eText1 = findViewById(R.id.riderName);
        eText2 = findViewById(R.id.riderNumber);
        eText3 = findViewById(R.id.riderPassword);
        //toolbar = (Toolbar)findViewById(R.id.toolbar);
        r_ImageView = findViewById(R.id.r_imageView);

        final String Name = getIntent().getStringExtra("User_Name");
        final String Phone = getIntent().getStringExtra("User_Phone");
        final String Password = getIntent().getStringExtra("User_Password");

       eText1.setText(Name);
       eText2.setText(Phone);
       eText3.setText(Password);





       /* r_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                runOnUiThread(new Runnable() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void run() {
                        new AsyncTask() {

                            @Override
                            protected Object doInBackground(Object[] objects) {


                                OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
                                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                                        .addFormDataPart("submit_image", app_users_images.getName(),RequestBody.create(MediaType.parse("images/jpeg"),"app_users_images"))
                                        .build();
                                Request request = new Request.Builder().url("http://192.168.1.208/userImages").post(body).build();
                                Response response = null;
                                try {
                                    response = client.newCall(request).execute();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    String result = response.body().string();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        }.execute();
                    }
                });

            }
        });*/
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }
}
