package techno.vourier.shipdotssailer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import techno.vourier.shipdotssailer.R;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

import static techno.vourier.shipdotssailer.MainActivity.cancelPickup;
import static techno.vourier.shipdotssailer.MainActivity.orderDetails;
import static techno.vourier.shipdotssailer.MainActivity.scan_btn;
import static techno.vourier.shipdotssailer.splashscreen.conor;
import static techno.vourier.shipdotssailer.splashscreen.stmt;

public class cancelpickup extends AppCompatActivity {

    TextView textViewZ;
    EditText EditBox;
    RadioGroup radioGroup;
    RadioButton radioButton1, radioButton2, radioButton3, radioButton4,other,radioButton;
    Button submitButton;
    String orderId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancelpickupfragment);

        EditBox = findViewById(R.id.editBox);
        textViewZ = findViewById(R.id.fragmenttext);
        //radioGroup = findViewById(R.id.radioCancel);
        radioButton1 = findViewById(R.id.radioCall);
        radioButton2 = findViewById(R.id.radioTommorow);
        radioButton3 = findViewById(R.id.radioPayment);
        radioButton4 = findViewById(R.id.radioCancellation);
        other = findViewById(R.id.radioOther);
       // submitButton = findViewById(R.id.radioButton);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("PickUp Not Successful");

        orderId = MainActivity.order_id;

        EditBox.setVisibility(View.GONE);

        addListenerOnButton();


        other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditBox.setVisibility(View.VISIBLE);


            }
        });

        radioButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditBox.setVisibility(View.GONE);

            }
        });

        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditBox.setVisibility(View.GONE);

            }
        });

        radioButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditBox.setVisibility(View.GONE);

            }
        });

        radioButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditBox.setVisibility(View.GONE);

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }


    public void addListenerOnButton() {

      radioGroup = (RadioGroup) findViewById(R.id.radioCancel);
      submitButton = (Button) findViewById(R.id.radioButton);

       submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);

                Toast.makeText(cancelpickup.this,
                        radioButton.getText(), Toast.LENGTH_LONG).show();

                String st = radioButton.getText().toString();
                Log.e("RADIO VAlUE",st);


                orderDetails.setVisibility(View.GONE);
                scan_btn.setVisibility(View.GONE);
                cancelPickup.setVisibility(View.GONE);

                new onFailedpickup().execute();

            }
        });

    }

    public class onFailedpickup extends  AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {

            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().build();
           // StrictMode.setThreadPolicy(policy);


            try {

                // Old Way of doind jdbc Statement Connection
                /*Class.forName("com.mysql.jdbc.Driver");
                conor = DriverManager.getConnection(Login.url,Login.username,Login.password);
                stmt = conor.createStatement();*/

                //stmt.executeUpdate("UPDATE customer_orders SET Status = 'FAILED' WHERE Order_ID = '"+orderId+"'");


                // Toast.makeText(cancelpickup.this,"Submitted",Toast.LENGTH_LONG).show();


               /* Class.forName("com.mysql.jdbc.Driver");
                conor = DriverManager.getConnection(Login.url,Login.username,Login.password);
                conor.setAutoCommit(false);
                */

                String st1 = "UPDATE customer_orders SET Status = 'FAILED' WHERE Order_ID = '"+orderId+"' ";
                stmt.executeUpdate(st1);


               //conor.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return null;
        }

       /* @Override
        protected void onPostExecute(Object o) {

            startActivity(new Intent(cancelpickup.this,MainActivity.class));
            super.onPostExecute(o);
        }

        */


    }
}

