package techno.vourier.shipdotssailer.Other_Activities;

class getsetOrderHistory {
    // int mapView1;
    String c_name,c_pay,c_address,c_date,c_phone,c_scanId,c_orderID;


    public getsetOrderHistory(String c_name, String c_pay, String c_address, String c_date, String c_phone, String c_scanId, String c_orderID) {
        //this.mapView1 = mapView1;
        this.c_name = c_name;
        this.c_pay = c_pay;
        this.c_address = c_address;
        this.c_date = c_date;
        this.c_phone = c_phone;
        this.c_scanId = c_scanId;
        this.c_orderID = c_orderID;
    }

   /*public int getMapView1() {
      return mapView1;
   }

   public void setMapView1(int mapView1) {
      this.mapView1 = mapView1;
   }*/

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_pay() {
        return c_pay;
    }

    public void setC_pay(String c_pay) {
        this.c_pay = c_pay;
    }

    public String getC_address() {
        return c_address;
    }

    public void setC_address(String c_address) {
        this.c_address = c_address;
    }

    public String getC_date() {
        return c_date;
    }

    public void setC_date(String c_date) {
        this.c_date = c_date;
    }

    public String getC_phone() {
        return c_phone;
    }

    public void setC_phone(String c_phone) {
        this.c_phone = c_phone;
    }

    public String getC_scanId() {
        return c_scanId;
    }

    public void setC_scanId(String c_scanId) {
        this.c_scanId = c_scanId;
    }

    public String getC_orderID() {
        return c_orderID;
    }

    public void setC_orderID(String c_orderID) {
        this.c_orderID = c_orderID;
    }
}
