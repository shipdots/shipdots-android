package techno.vourier.shipdotssailer.Other_Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import techno.vourier.shipdotssailer.MainActivity;
import techno.vourier.shipdotssailer.R;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

import static techno.vourier.shipdotssailer.splashscreen.conor;
import static techno.vourier.shipdotssailer.splashscreen.stmt;

public class order_history extends AppCompatActivity {

    ArrayList<getsetOrderHistory> historyList;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter mAdapter;
    String c_name,c_payment,c_address,c_phone,c_scanId,c_order_ID,c_time,rider_phone;

    RecyclerView recyclerView;
    TextView dateText,payment_method;
    CardView view;

    ArrayList<String> Arrayhistory = new ArrayList<>();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("History");

        recyclerView = findViewById(R.id.rv);
        dateText = findViewById(R.id.cvDate);
        payment_method = findViewById(R.id.cvPayment);
        view = findViewById(R.id.cv_history);

        rider_phone = MainActivity.final_phone;

        historyList = new ArrayList<>();


        new history().execute();

                            /* Calendar calendar = Calendar.getInstance();
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-mm-yyyy");
       String dateTime = simpleDateFormat.format(calendar.getTime());
       dateText.setText(dateTime);
        Log.e("Time is not Good:",date);

       */

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }



    @SuppressLint("StaticFieldLeak")
    public class history extends AsyncTask {
        ProgressDialog p1;

        @Override
        protected void onPreExecute() {
            p1 = new ProgressDialog(order_history.this);
            p1.setMessage("Opening History");
            p1.setIndeterminate(false);
            p1.setCancelable(false);
            p1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p1.show();

            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

          //  StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
           // StrictMode.setThreadPolicy(policy);

            try {
                /*Class.forName("com.mysql.jdbc.Driver");
                conor = DriverManager.getConnection(Login.url, Login.username, Login.password);
                stmt = conor.createStatement();

                 */
                //String string = "SELECT * FROM customer_orders WHERE Rider_Number =?  ORDER BY Date_And_Time DESC";
                String st1 = "SELECT * FROM customer_orders WHERE Rider_Number = '"+rider_phone+"' ORDER BY Date_And_Time DESC";
                ResultSet setting = stmt.executeQuery(st1);

                while (setting.next()) {

                    Arrayhistory.add(setting.getString("UID") + "@" + setting.getString("Order_ID")  + "@" + setting.getString("sender_address_line_1") + "@" + setting.getString("sender_address_line_2") + "@" + setting.getString("Payment_Mode") + "@" + setting.getString("Price_Paid_By_Customer") + "@" + setting.getString("Date_And_Time") + "@" + setting.getString("Sender_Name") + "@" + setting.getString("sender_mobile"));
                    Log.e("New Result", String.valueOf(Arrayhistory));
                }

              // conor.close();
            } catch (SQLException e) {

                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Object o) {

            p1.dismiss();

           for (String hs : Arrayhistory) {
                String uid = hs.split("@")[0];
                String id = hs.split("@")[1];
                String ad1 = hs.split("@")[2];
                String ad2 = hs.split("@")[3];
                String pm = hs.split("@")[4];
                String ppbc = hs.split("@")[5];
                String dt = hs.split("@")[6];
                String sn = hs.split("@")[7];
                String sm = hs.split("@")[8];


               Log.e("Sender Name",sn);
               Log.e("UID",uid);


             c_scanId = uid;
             c_address = ad1.concat(ad2);

             if(pm.equals("Online") || pm.equals("online") || pm.equals("ONLINE")){
                 c_payment = pm;
                // payment_method.setText(c_payment);
                // payment_method.setBackgroundResource(R.color.paymentOnline);

             }else{
                 c_payment = ppbc;
                // payment_method.setText(c_payment);
                // payment_method.setTextColor(Color.parseColor("#FFFFFF"));
                // payment_method.setBackgroundResource(R.color.paymentCash);
             }
             c_time = dt;
             c_name = sn;
             c_phone = sm;
             c_order_ID = id;

             Log.e("c_name",c_name);

             historyList.add(new getsetOrderHistory(c_name,c_payment,c_address,c_time,c_phone,c_scanId,c_order_ID));
             Log.e("History List", String.valueOf(historyList));

               recyclerView.setNestedScrollingEnabled(false);
               recyclerView.setHasFixedSize(false);
               layoutManager = new LinearLayoutManager(order_history.this);
               mAdapter = new Adapterhistory(historyList);
               recyclerView.setLayoutManager(layoutManager);
               recyclerView.setAdapter(mAdapter);
           }

            super.onPostExecute(o);


                }

            }


        }



