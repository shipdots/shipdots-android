package techno.vourier.shipdotssailer;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import techno.vourier.shipdotssailer.R;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;



import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static techno.vourier.shipdotssailer.splashscreen.conor;
import static techno.vourier.shipdotssailer.splashscreen.stmt;


public class Registration extends AppCompatActivity {
    private static final String TAG = "Registration";

    Button register,login;
    EditText p_number,p_name,p_password,p_acard,p_dl;
    TextView bname;
    String UserName,UserPhone,UserPassword,UserAadharCard,UserDriversLicence;
    File ids1,ids2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Log.d(TAG,"Register Page Opens");

        bname = findViewById(R.id.textView3);

        p_number = findViewById(R.id.mobileNumber);
        p_password = findViewById(R.id.password);
        p_name = findViewById(R.id.name);
        p_acard = findViewById(R.id.ACard);
        p_dl = findViewById(R.id.DLicence);

        register = findViewById(R.id.register);
        login = findViewById(R.id.loginButton);

        p_acard.setClickable(false);
        p_acard.setKeyListener(null);
        p_dl.setClickable(false);
        p_dl.setKeyListener(null);


        p_acard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
               startActivityForResult(i,10);
            }
        });


        p_dl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i,11);
            }
        });





       register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserName = p_name.getText().toString();
                UserPhone = p_number.getText().toString();
                UserPassword = p_password.getText().toString();
                UserAadharCard = p_acard.getText().toString();
                UserDriversLicence = p_dl.getText().toString();

                ids1 = new File(UserAadharCard);
                ids2 = new File(UserDriversLicence);




                new registration().execute();


                register.setEnabled(true);
                register.setClickable(true);
                //register.setAlpha(.5f);
               //login.setVisibility(View.GONE);




            }
        });



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Registration.this,Login.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            // String picturePath contains the path of selected Image

            p_acard.setText(picturePath);
        }

        if (requestCode == 11 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            // String picturePath contains the path of selected Image

            p_dl.setText(picturePath);
        }
    }
   @SuppressLint("StaticFieldLeak")
    public class registration extends AsyncTask{
       ProgressDialog p1;
       int counter=0;


       protected void onPreExecute(){
           p1 = new ProgressDialog(Registration.this);
           p1.setMessage("Registration In Process");
           p1.setCancelable(false);
           p1.setIndeterminate(true);
           p1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
           p1.show();
           super.onPreExecute();

       }

        @Override
        protected Object doInBackground(Object[] objects) {
           Log.e("Start","we dont know this working");

            if(UserName.isEmpty() || UserPhone.isEmpty()|| UserPassword.isEmpty() || UserAadharCard.isEmpty()|| UserDriversLicence.isEmpty() ){
               counter = -200;

            }else{
                if(checkforRegistration()){
                  counter = -100;

                } else{
                    try {
                       /* Class.forName("com.mysql.jdbc.Driver");
                        Connection con = DriverManager.getConnection(Login.url,Login.username,Login.password);
                        Statement st = con.createStatement();
                        st.executeUpdate("INSERT INTO unauthorized_riders (Name,Number,Password,aadhar_url,driver_licence_url)"  +"VALUES('"+UserName+"','"+UserPhone+"','"+UserPassword+"','"+UserAadharCard+"','"+UserDriversLicence+"','0')");
                        Toast.makeText(Registration.this, "Account Created.", Toast.LENGTH_SHORT).show();

                        */
                       OkHttpClient  client  = new OkHttpClient.Builder().connectTimeout(10,TimeUnit.SECONDS).writeTimeout(180,TimeUnit.SECONDS).readTimeout(180,TimeUnit.SECONDS).build();
                       RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                               .addFormDataPart("username",UserName)
                               .addFormDataPart("password",UserPassword)
                               .addFormDataPart("phonenumber",UserPhone)
                               .addFormDataPart("submit_image",ids1.getName(),RequestBody.create(MediaType.parse("image/jpeg"),ids1))
                               .addFormDataPart("submit_image2",ids2.getName(),RequestBody.create(MediaType.parse("image/jpeg"),ids2))
                               .build();
                       Request request = new Request.Builder().url("https://app.vourier.com/rider_scripts/rider_registration.php").post(body).build();
                       Response response = null;
                        try {
                            response = client.newCall(request).execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } try{
                            String result = response.body().string();

                            Log.e("Response",result);

                        }catch (IOException e){
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
          /*  runOnUiThread(new Runnable() {
                @SuppressLint("StaticFieldLeak")
                @Override
                public void run() {
                    new AsyncTask() {

                        @Override
                        protected Object doInBackground(Object[] objects) {


                            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
                            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                                    .addFormDataPart("username",UserName)
                                    .addFormDataPart("password",UserPassword)
                                    .addFormDataPart("phonenumber",UserPhone)
                                    .addFormDataPart("submit_image", ids1.getName(),RequestBody.create(MediaType.parse("images/jpeg"),ids1))
                                    .addFormDataPart("submit_image",ids2.getName(),RequestBody.create(MediaType.parse("images/jpeg"),ids2))
                                    .build();
                            Request request = new Request.Builder().url("http://18.219.17.27/AdminLTE/rider_scripts/rider_ids/").post(body).build();
                            Response response = null;
                            try {
                                response = client.newCall(request).execute();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            try {
                                String result = response.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    }.execute();

                }
            });*/

            return null;
        }



       protected void onPostExecute(Object o){
            p1.dismiss();
          //  if (counter>0){ }


            if(counter == -100){
                login.setVisibility(View.VISIBLE);
                Toast.makeText(Registration.this,"Number is already Registered",Toast.LENGTH_LONG).show();
            }
            if(counter == -200){
                login.setVisibility(View.VISIBLE);
                Toast.makeText(Registration.this, "All fields Required", Toast.LENGTH_LONG).show();

            }else {
                login.setVisibility(View.GONE);
                Toast.makeText(Registration.this,"Registered Successfully",Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(Registration.this,Login.class);
                startActivity(intent1);

            }


           super.onPostExecute(o);

       }
    }

    public boolean checkforRegistration(){

        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);

        try {
           /* Class.forName("com.mysql.jdbc.Driver");
            conor = DriverManager.getConnection(Login.url,Login.username,Login.password);
            stmt = conor.createStatement();
            */

            String coco = "SELECT * from unauthorized_riders where NUMBER ='"+UserPhone+"'";
            //PreparedStatement preparedStatement = conor.prepareStatement(coco);
            //preparedStatement.setString(1,UserPhone);
            ResultSet setting = stmt.executeQuery(coco);




          //  if(rst.next()) {
                Log.e("checking","this is mis-happening ");
                return setting.next();

           // }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }


}
