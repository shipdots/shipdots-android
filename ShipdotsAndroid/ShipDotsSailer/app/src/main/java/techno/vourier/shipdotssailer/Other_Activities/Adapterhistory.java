package techno.vourier.shipdotssailer.Other_Activities;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import techno.vourier.shipdotssailer.R;

import java.util.ArrayList;


class Adapterhistory extends RecyclerView.Adapter<Adapterhistory.ViewHolder> {

    // private Context mContext;
    private ArrayList<getsetOrderHistory> aList;


    public Adapterhistory(ArrayList<getsetOrderHistory>list){
        //mContext = context;
        aList = list;

    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_cardview, parent, false);
        return  new ViewHolder(view);
    }

    @NonNull
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        getsetOrderHistory orderHistory = aList.get(position);

        //MapView mapView1 = holder.mapView;

        TextView c_name,c_pay,c_address,c_phone,c_date,c_scanId,c_orderID;

        c_name = holder.nameText;
        c_pay = holder.payText;
        c_address = holder.addressText;
        c_phone = holder.phoneText;
        c_date = holder.dateText;
        c_scanId = holder.scanIdText;
        c_orderID = holder.orderIdText;


        c_name.setText(orderHistory.getC_name());
        c_pay.setText(orderHistory.getC_pay());
        c_address.setText(orderHistory.getC_address());
        c_phone.setText(orderHistory.getC_date());
        c_date.setText(orderHistory.getC_phone());
        c_scanId.setText(orderHistory.getC_scanId());
        c_orderID.setText(orderHistory.getC_orderID());



    }

    @Override
    public int getItemCount() {

        return aList.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder  {

        public TextView nameText,payText,addressText,phoneText,dateText,scanIdText,orderIdText;
        // public MapView mapView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            nameText =  itemView.findViewById(R.id.cvname);
            payText = itemView.findViewById(R.id.cvPayment);
            addressText = itemView.findViewById(R.id.cvAddress);
            phoneText = itemView.findViewById(R.id.cvPhone);
            dateText = itemView.findViewById(R.id.cvDate);
            scanIdText = itemView.findViewById(R.id.cvScanUID);
            orderIdText = itemView.findViewById(R.id.cvOrderId);
            // mapView = itemView.findViewById(R.id.cvMap);



        }
    }

        /*@Override
        public void onMapReady(GoogleMap googleMap) {

        }*/
}


