package techno.vourier.shipdotssailer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.Result;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static techno.vourier.shipdotssailer.MainActivity.cancelPickup;
import static techno.vourier.shipdotssailer.MainActivity.orderDetails;
import static techno.vourier.shipdotssailer.MainActivity.scan_btn;


public class barcodescan extends AppCompatActivity implements ZXingScannerView.ResultHandler {

   public static String scan = "";
   String orderID,Rider_Phone;


   ZXingScannerView zXingScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        zXingScannerView = new ZXingScannerView(this);
        setContentView(zXingScannerView);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("SCAN");

        orderID = MainActivity.OrderId.getText().toString();
        Log.e("orderID in Barcode",orderID);
        Rider_Phone = MainActivity.final_phone;
        Log.e("Phone in Barcode",Rider_Phone);



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);
        return true;
    }

    @Override
    public void handleResult(Result result) {

            String str1 = result.getText().substring(0,2);

            if(str1.equals("11")){

                    MainActivity.textBarcode.setText(result.getText());
                    //OKHTTP SCRIPT TO UPDATE UID IN DATABASE-->RIDER HISTORY TABLE WILL BE UPDATED
                //Log.e("Scan1:",result.getText());
                scan = result.getText();
                Log.e("scan2",scan);
                Toast.makeText(barcodescan.this,"Pick UP Successful",Toast.LENGTH_LONG).show();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AsyncTask(){

                        @Override
                        protected Object doInBackground(Object[] objects) {

                            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10,TimeUnit.SECONDS).readTimeout(10,TimeUnit.SECONDS).build();
                            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                                    .addFormDataPart("Scan_UID",scan)
                                    .addFormDataPart("Order_IDs",orderID)
                                    .addFormDataPart("Phone",Rider_Phone)
                                    .build();

                            Request request = new Request.Builder().url("https://app.vourier.com/rider_scripts/rider_order_history.php").post(body).build();
                            Response response = null;
                            try {
                                response = okHttpClient.newCall(request).execute();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }try{
                                String result1 = response.body().string();

                                Log.e("Response3",result1);

                            }catch (IOException e){
                                e.printStackTrace();
                            }

                            return null;
                        }
                    }.execute();
                }
            });

                orderDetails.setVisibility(View.GONE);
                scan_btn.setVisibility(View.GONE);
                cancelPickup.setVisibility(View.GONE);

            }else{

                Toast.makeText(barcodescan.this,"Wrong Code\n Scan Again",Toast.LENGTH_LONG).show();
            }


             onBackPressed();


    }

    @Override
    protected void onPause() {
        super.onPause();

        zXingScannerView.stopCamera();
    }


    @Override
    protected void onResume() {
        super.onResume();
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }

 }



