package techno.vourier.shipdotssailer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import techno.vourier.shipdotssailer.Other_Activities.ViewImage;
import techno.vourier.shipdotssailer.Other_Activities.order_history;
import techno.vourier.shipdotssailer.Other_Activities.rider_day_summary;
import techno.vourier.shipdotssailer.Other_Activities.rider_info;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.android.material.snackbar.Snackbar;
import com.mysql.jdbc.Statement;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import techno.vourier.shipdotssailer.R;


import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static techno.vourier.shipdotssailer.splashscreen.conor;
import static techno.vourier.shipdotssailer.splashscreen.stmt;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    File login = new File(Environment.getExternalStorageDirectory()+"/Android/data/ShipDotsSailer/login.txt");
    File date = new File(Environment.getExternalStorageDirectory()+"/Android/data/ShipDotsSailer/date.txt");
    CircleImageView navBarImage, navHeaderImage;
    private Uri cropImageURI;
    GoogleMap map;
    Location lastLocation;
    private LocationCallback locationCallback;
    FusedLocationProviderClient fusedLocationProviderClient;
    public PreparedStatement statement;


    public static TextView nameText, addressText, mode_of_payment, OrderId,textBarcode,textPhone,NavUserName,NavUserPhone;
    ArrayList<String> orderIDs = new ArrayList<>();
    public  static CardView orderDetails;
    public static Button trackButton, scan_btn, cancelPickup;
    String latitude, longitude = "";
    View mapView;
    public static String final_phone ="" ,final_password ="",rider_NAME;
    public  static String pickupBoy_ID,cash,order_id;
    public CountDownTimer cdtimer;
    private Uri mCropImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);

        navBarImage = findViewById(R.id.navBarImage);


        //calling layout
        textPhone = findViewById(R.id.textNumber);


        nameText = findViewById(R.id.textName);
        scan_btn = findViewById(R.id.scan);
        cancelPickup = findViewById(R.id.pickCancel);


        //CardView layout
        orderDetails = findViewById(R.id.card1);
        trackButton = findViewById(R.id.trackButton);
        addressText = findViewById(R.id.textAddress);
        mode_of_payment = findViewById(R.id.textpayMode);
        OrderId = findViewById(R.id.textOrderId);
        textBarcode = findViewById(R.id.textBarcode);


        orderDetails.setVisibility(View.GONE);
        scan_btn.setVisibility(View.GONE);
        cancelPickup.setVisibility(View.GONE);


        if(!date.exists()){
            try{
           date.createNewFile();
                FileWriter fileWriter2 = new FileWriter(date,false);
                fileWriter2.write(OrderId.toString());
                fileWriter2.write(" ");
                fileWriter2.write(mode_of_payment.toString());
                fileWriter2.write(" ");
                fileWriter2.write(cash);
                fileWriter2.write(" ");
                //fileWriter2.write(date);

            }catch (Exception e){
                e.printStackTrace();
            }
        }


      // creating user text file
       final String Phone = getIntent().getStringExtra("Phone");
       final String Password = getIntent().getStringExtra("Password");
       //final String RIDER_HUB_ID = getIntent().getStringExtra("Pickupboy_ID");
        //Log.e("pickupBoy_ID",RIDER_HUB_ID);


        if(!login.exists()){
           try {
               login.createNewFile();
               FileWriter fileWriter = new FileWriter(login, false);
               assert Phone != null;
               fileWriter.write(Phone);
               fileWriter.write(" ");
               assert Password != null;
               fileWriter.write(Password);
               fileWriter.flush();
               fileWriter.close();




           } catch (Exception e) {
               e.printStackTrace();
           }
       }
        final_phone = Phone;
        final_password = Password;
        //final_pickupBoy_ID = RIDER_HUB_ID;
        //Log.e("pickup_boy_id",final_pickupBoy_ID);


        //nav header details
        HashMap<String, String> riderDetails = getRiderDetails();

        rider_NAME = riderDetails.get("NAME");
        //Log.e("Name Checking:",rider_NAME);

        pickupBoy_ID = riderDetails.get("HUB_RIDER_ID");
//        Log.e("id for rider",pickupBoy_ID);
        //final_pickupBoy_ID = pickupBoy_ID;


        final String ImageLink = riderDetails.get("IMAGE_LINK");
        //Log.e("Image_Link",ImageLink);

        //final String ImageLink2 = riderDetails.get("IMAGE_LINK");
//        Log.e("ImageLink2",ImageLink2);



        View nav_view = navigationView.getHeaderView(0);

        navHeaderImage = nav_view.findViewById(R.id.imageView);


        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable(){

            @Override
            public void run() {
                Picasso.with(getBaseContext()).load(ImageLink).networkPolicy(NetworkPolicy.NO_STORE).into(navHeaderImage);
                //Picasso.with(getBaseContext()).load(ImageLink2).networkPolicy(NetworkPolicy.NO_CACHE).into(navBarImage);
            }
        });


        NavUserName = nav_view.findViewById(R.id.navUserName);
        NavUserName.setText(rider_NAME);

        NavUserPhone = nav_view.findViewById(R.id.navUserPhone);
        NavUserPhone.setText(Phone);



        cancelPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              Intent intent2 = new Intent(MainActivity.this,cancelpickup.class);
              startActivity(intent2);

            }
        });


        textPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                makePhoneCall();
            }
        });


        scan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mode_of_payment.getText().equals("Online") && !mode_of_payment.getText().equals("ONLINE") && !mode_of_payment.getText().equals("online")) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("CASH REMINDER")
                            .setMessage("Collect Cash\n Rs: "+cash+"/-")
                            .setIcon(R.drawable.cash_in_hand)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    startActivity(new Intent(MainActivity.this, barcodescan.class));

                                }
                            }).create().show();
                } else {
                    startActivity(new Intent(MainActivity.this, barcodescan.class));
                }

            }
        });

        navBarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

            new FetchDataAndMark().execute();

        trackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTrack(latitude, longitude);
            }
        });


        //COUNT DOWN TIMER

       cdtimer  = new CountDownTimer(10000,10000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                try{
                   // map.clear();
                    new FetchDataAndMark().execute();

                }catch(Exception e){
                    Log.e("Error", "Error: " + e.toString());
                }
            }


        }.start();




       /* navHeaderImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new AsyncTask() {

                            @Override
                            protected Object doInBackground(Object[] objects) {


                                OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
                                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                                        .addFormDataPart("submit_image", path.getName(),RequestBody.create(MediaType.parse("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),path))
                                        .build();
                                Request request = new Request.Builder().url("http://192.168.1.208/C:\\Users\\Ansh Computer\\Documents").post(body).build();
                                Response response = client.newCall(request).execute();
                                result = response.body().string();
                                return null;
                            }
                        }.execute();
                    }
                });
            }
        });*/

       navHeaderImage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.myDialog)).setTitle("Select An Option").setMessage("What do you want to do?")
                       .setPositiveButton("View Image", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                               Intent showImage = new Intent(MainActivity.this, ViewImage.class);
                               showImage.putExtra("URL", ImageLink);
                               startActivity(showImage);
                           }
                       })
                       .setNegativeButton("Change Image", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                               changeImage();

                           }
                       }).create().show();
           }
       });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
       // } else {
       //    super.onBackPressed();
        }

        orderDetails.setVisibility(View.GONE);
        scan_btn.setVisibility(View.GONE);
        cancelPickup.setVisibility(View.GONE);

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Are you sure you want to exit")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.this.finishAffinity();


                    }
                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       dialogInterface.cancel();
                    }
                }).create().show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_rSummary) {
            startActivity(new Intent(MainActivity.this, rider_day_summary.class ));


        } else if (id == R.id.nav_history) {
            startActivity(new Intent(MainActivity.this, order_history.class));

        } else if (id == R.id.nav_info) {
            Intent r_intent = new Intent(MainActivity.this, rider_info.class);
            r_intent.putExtra("User_Name", rider_NAME);
            r_intent.putExtra("User_Phone",final_phone);
            startActivity(r_intent);

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_chat) {

        } else if (id == R.id.nav_legal) {

        } else if (id == R.id.nav_logout) {

              new AlertDialog.Builder(MainActivity.this)
                      .setTitle("Logout")
                      .setIcon(R.drawable.icon_logout_new)
                      .setMessage("Are you sure you want to logout")
                      .setCancelable(true)
                      .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialogInterface, int i) {
                              if(login.exists()){
                                  login.delete();
                                  Log.e("Logout Successfull","this is working");
                                  Toast.makeText(MainActivity.this,"Logged Out",Toast.LENGTH_SHORT).show();
                                  startActivity(new Intent(MainActivity.this,Login.class));
                                  finish();

                              }

                          }
                      })
                      .setNegativeButton("No", new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialogInterface, int i) {
                              dialogInterface.cancel();
                          }
                      }).create().show();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public HashMap getRiderDetails() {
        final HashMap<String, String> user_details = new HashMap<>();

        runOnUiThread(new Runnable(){
            public void run(){


           StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
           StrictMode.setThreadPolicy(policy);
        try {
           //Class.forName("com.mysql.jdbc.Driver");
           //Connection cono = DriverManager.getConnection(splashscreen.url, splashscreen.username, splashscreen.password);



                String string2 = "SELECT * FROM pickup_rider_details WHERE RIDER_NUMBER= '"+final_phone+"' AND PASSWORD ='"+final_password+"'" ;
                if(conor!=null) {

                ResultSet set = stmt.executeQuery(string2);

                if (set.next()) {
                    set.beforeFirst();

                    while (set.next()) {

                        user_details.put("NAME", set.getString("NAME"));
                        user_details.put("HUB_RIDER_ID", set.getString("HUB_RIDER_ID"));
                        //user_details.put("IMAGE_LINK", set.getString("ImageLink"));

                    }
                } else {
                    if (login.exists()) {
                        if (login.delete()) {
                            Toast.makeText(MainActivity.this, "You've been Logged Out. Login Again!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, Login.class));
                            finish();
                        }
                    }
                }
                String stnew = "SELECT ImageLink as ImageLink FROM authorized_riders WHERE Registered_Number = '"+final_phone+"' ";
                set = stmt.executeQuery(stnew);

                ResultSetMetaData metaData = set.getMetaData();
                for(int index = 1; index<=metaData.getColumnCount();index++){
                    System.out.println("Column "+index+" is named "+metaData.getColumnName(index));
                }

                if (set.next()) {
                    set.beforeFirst();
                    while (set.next()) {
                        user_details.put("IMAGE_LINK", set.getString("ImageLink"));


                    }
                }
            }
            //conor.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
            }
        });
        return user_details;
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);


        //current location button
        if(mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40,250);
        }

        //check if GPS is enabled
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(MainActivity.this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(MainActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getDeviceLocation();
            }
        });

        task.addOnFailureListener(MainActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        resolvable.startResolutionForResult(MainActivity.this, 51);
                    }
                    catch (IntentSender.SendIntentException i) {
                        i.printStackTrace();
                    }
                }
            }
        });



        googleMap.setOnMarkerClickListener(this);


        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.mapstyle));

            if (!success) {
                Log.e("MapActivity", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapActivity", "Can't find style. Error: ", e);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        //cdtimer.start();
        if(marker.equals("1")) {
            marker.setVisible(true);
            //marker.setKeyListener(null);
        } else {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("CAUTION")
                    .setIcon(R.drawable.icon_stop)
                    .setMessage("Are you sure you want to change location and order")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            orderDetails.setVisibility(View.VISIBLE);
                            scan_btn.setVisibility(View.VISIBLE);
                            cancelPickup.setVisibility(View.VISIBLE);



                            String name = marker.getTitle().split("`")[0];
                            String address1 = marker.getTitle().split("`")[1];
                            String address2 = marker.getTitle().split("`")[2];
                            String mop = marker.getTitle().split("`")[3];
                            cash = marker.getTitle().split("`")[4];
                            String number = marker.getTitle().split("`")[5];
                            order_id = marker.getTitle().split("`")[6];

                            LatLng position = marker.getPosition();
                            latitude = String.valueOf(position.latitude);
                            longitude = String.valueOf(position.longitude);


                            nameText.setText(name);
                            addressText.setText(address1);
                            addressText.append(address2);
                            OrderId.setText(order_id);
                            //mode_of_payment.setText(mop);
                            textPhone.setText(number);


                            if(mop.equals("ONLINE") || mop.equals("online") || mop.equals("Online")){
                                mode_of_payment.setText(mop);
                                mode_of_payment.setBackgroundResource(R.color.paymentOnline);

                            }else {
                                mode_of_payment.setText("Rs."+cash);
                                mode_of_payment.setTextColor(Color.parseColor("#FFFFFF"));
                                mode_of_payment.setBackgroundResource(R.color.paymentCash);
                            }




                            Toast.makeText(MainActivity.this,"Permission Granted",Toast.LENGTH_SHORT).show();

                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    }).create().show();



        }
        // Empty the SCAN FIELD TextView
        if(!marker.equals("1") && !textBarcode.toString().isEmpty()){
            textBarcode.setText("");
        }

        return true;
    }



    public void startTrack(String latitude, String longitude) {
        orderDetails.setVisibility(View.VISIBLE);
        scan_btn.setVisibility(View.VISIBLE);
        cancelPickup.setVisibility(View.VISIBLE);



        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr="+latitude+","+longitude+""));
        startActivity(intent);

    }



    @SuppressLint("StaticFieldLeak")
    public class FetchDataAndMark extends AsyncTask {

        //private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            orderIDs = new ArrayList<>();


            try {
               //Class.forName("com.mysql.jdbc.Driver");
               //Connection cono1 = DriverManager.getConnection(splashscreen.url, splashscreen.username, splashscreen.password);

                    String sql1 = "Select * FROM customer_orders WHERE Status = 'PROCESSING' AND Pickupboy_ID = '"+pickupBoy_ID +"' ";
                    ResultSet setting =  stmt.executeQuery(sql1);


                while (setting.next()) {
                    Log.e("!", "!");
                    orderIDs.add(setting.getString("Order_ID") + "@" + setting.getString("Latitude") + "@" + setting.getString("Longitude") + "@" + setting.getString("sender_address_line_1")+"@" +setting.getString("sender_address_line_2")+ "@" + setting.getString("Payment_Mode") + "@" + setting.getString("Price_Paid_By_Customer") + "@" + setting.getString("Sender_Name")+ "@" + setting.getString("sender_mobile"));
                    Log.e("Result",String.valueOf(orderIDs));
                }

               //cono.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            map.clear();

            Log.e("markers", "TEST --> "+String.valueOf(orderIDs));

            for(String orders: orderIDs) {
                String ID = orders.split("@")[0];
                String Latitude = orders.split("@")[1];
                String Longitude = orders.split("@")[2];
                String Address1 = orders.split("@")[3];
                String Address2 = orders.split("@")[4];
                String payment_mode = orders.split("@")[5];
                String cash_paid = orders.split("@")[6];
                String Name = orders.split("@")[7];
                String Number = orders.split("@")[8];

                map.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Latitude), Double.parseDouble(Longitude))).title(Name+"`"+Address1+"`"+Address2+"`"+payment_mode+"`"+cash_paid+"`"+Number+"`"+ID));
               }
            cdtimer.start();

        }

    }

    //phone
    @SuppressLint("MissingPermission")
    private void makePhoneCall() {
        String number = textPhone.getText().toString();
        if (number.trim().length() > 0) {

                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 51) {
            if(resultCode == RESULT_OK) {
                getDeviceLocation();
            }
        }
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getBaseContext(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getBaseContext(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already granted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                ((CircleImageView) findViewById(R.id.imageView)).setImageURI(result.getUri());
                Bitmap photo = ((BitmapDrawable)navHeaderImage.getDrawable()).getBitmap();
                new uploadImage().execute(photo);
                Toast.makeText(getBaseContext(), "Image Successfully Set!", Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getBaseContext(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }


   //current location
    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        fusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if(task.isSuccessful()) {
                            lastLocation = task.getResult();
                            if(lastLocation != null) {
                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), 14));
                            } else {
                                final LocationRequest locationRequest = LocationRequest.create();
                                locationRequest.setInterval(10000);
                                locationRequest.setFastestInterval(5000);
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationCallback = new LocationCallback() {
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if(locationResult == null){
                                            return;
                                        }
                                        else {
                                            lastLocation = locationResult.getLastLocation();
                                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), 14));
                                            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                                        }
                                    }
                                };
                                fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        }
                        else {
                            Toast.makeText(MainActivity.this, "Unable to get Last Location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    public void changeImage() {
        startActivityForResult(CropImage.getPickImageChooserIntent(getBaseContext()), CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(getBaseContext(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    @SuppressLint("StaticFieldLeak")
    public class uploadImage extends AsyncTask<Bitmap, Void, Void> {

        private ProgressDialog p;
        String message = "";

        @Override
        protected void onPreExecute() {
            p = new ProgressDialog(MainActivity.this);
            p.setMessage("Updating Image..");
            p.setIndeterminate(false);
            p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            p.setCancelable(false);
            p.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Bitmap... bitmaps) {

            Bitmap image = bitmaps[0];

            String imageName = final_phone+".jpg";
            OutputStream os = null;
            try {
                os = new BufferedOutputStream(new FileOutputStream(Environment.getExternalStorageDirectory()+"/Android/data/ShipDotsSailer/"+imageName));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            image.compress(Bitmap.CompressFormat.JPEG, 60, os);

            File file = new File(Environment.getExternalStorageDirectory()+"/Android/data/ShipDotsSailer/"+imageName);

            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
            RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("submit_image", file.getName(), RequestBody.create(MediaType.parse("image/jpeg"), file)).build();
            Request request = new Request.Builder().url("https://app.vourier.com/rider_upload_image.php").post(requestBody).build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if(response.body() != null) {
                    String uploadResponse = response.body().string();
                    Log.e("RESPONSE", uploadResponse);
                    if (uploadResponse.equals("done")) {
                        message = "Profile picture changed";
                        deleteCache(MainActivity.this);
                    }
                    else {
                        message = "Upload failed";
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            p.dismiss();
            final DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            Snackbar.make(drawerLayout, message, Snackbar.LENGTH_LONG).show();
            super.onPostExecute(aVoid);
        }

        void deleteCache(Context context) {
            try {
                File dir = context.getCacheDir();
                deleteDir(dir);
            } catch (Exception e) { e.printStackTrace();}
        }

        boolean deleteDir(File dir) {
            if (dir != null && dir.isDirectory()) {
                String[] children = dir.list();
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
                return dir.delete();
            } else if(dir!= null && dir.isFile()) {
                return dir.delete();
            } else {
                return false;
            }
        }
    }


}
