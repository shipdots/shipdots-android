package techno.vourier.shipdotssailer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import techno.vourier.shipdotssailer.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import static techno.vourier.shipdotssailer.splashscreen.conor;
import static techno.vourier.shipdotssailer.splashscreen.password;
import static techno.vourier.shipdotssailer.splashscreen.stmt;
import static techno.vourier.shipdotssailer.splashscreen.url;
import static techno.vourier.shipdotssailer.splashscreen.username;

public class Login extends AppCompatActivity {

    //String[] appPermissions = {Manifest.permission.CALL_PHONE,Manifest.permission.CAMERA,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE};

    EditText editText, editText2;
    TextView textView, textView2, textView3;
    Button button;
    private static final int PERMISSION_REQUEST_CODE=10;

    private static final String TAG = "Login";
    /*public static String url = "jdbc:mysql://app.vourier.com/logistics_v2?useSSL=false";
    public static String username = "admin";
    public static String password = "Ankur123@";
    public static Connection conor;
    public static Statement stmt;
    public  static ResultSet setting = null;
    */

    public static String phoneNumber, passkey = "";

   // String assignedRiderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


       askForPermissions();

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText3);
        textView = findViewById(R.id.textView3);
        textView2 = findViewById(R.id.textView4);
        textView3 = findViewById(R.id.textView5);
        button = findViewById(R.id.button);




        //Saving Login Credentials
        // checking if folder exists
      File folder = new File(Environment.getExternalStorageDirectory()+"/Android/data/ShipDotsSailer/");
        if(!folder.exists()) {
            try {
                if(folder.mkdirs())
                    Log.e("YES","1");
                else
                    Log.e("NO", "2");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }



        String[] details = checkForPasswordFile();
        if(details != null) {
            String unique_id = details[0];
            String password = details[1];


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(Login.this, "Logging in", Toast.LENGTH_LONG).show();
                }
            });
            Intent goToMainPage = new Intent(Login.this, MainActivity.class);
            goToMainPage.putExtra("Phone", unique_id);
            goToMainPage.putExtra("Password", password);
            startActivity(goToMainPage);
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vibrate();

                phoneNumber = editText.getText().toString();
                passkey = editText2.getText().toString();

                new Logintask().execute();
            }
        });


        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,Registration.class));

            }
        });

    }

    private void askForPermissions() {
        try {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.CALL_PHONE,Manifest.permission.CAMERA},101);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("StaticFieldLeak")
    public class Logintask extends AsyncTask {
       ProgressDialog p;
       int counter=0;

       @Override
       protected void onPreExecute(){
           p = new ProgressDialog(Login.this);
           p.setMessage("Logging In");
           p.setIndeterminate(false);
           p.setCancelable(false);
           p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
           p.show();
           super.onPreExecute();

       }

        @Override
        protected Object doInBackground(Object[] objects) {

           StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try {
                OkHttpClient clientLogin = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10,TimeUnit.SECONDS).readTimeout(180,TimeUnit.SECONDS).build();
                RequestBody body2 = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("assign_rider_id","ASSIGN")
                        .addFormDataPart("registered_id",phoneNumber)
                        .addFormDataPart("registered_password",passkey)
                        .build();
                Request request2 = new Request.Builder().url("https://app.vourier.com/rider_scripts/dynamic_rider_id_assignment.php").post(body2).build();
                Response response2 = null;
                try {
                    response2 = clientLogin.newCall(request2).execute();
                    //Log.e("IF any ",String.valueOf(response2));
                } catch (Exception e) {
                    e.printStackTrace();
                } try{
                    String result = response2.body().string();

                    Log.e("Response2",result);



                }catch (IOException e){
                    e.printStackTrace();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                /*Class.forName("com.mysql.jdbc.Driver");
                 //conor = DriverManager.getConnection(url,username,password);
                // conor.setAutoCommit(false);
                 //stmt = conor.createStatement();
                 */
                //String st1 = "SELECT * FROM pickup_rider_details WHERE RIDER_NUMBER =? AND Password =?" ;


                String st1 = "SELECT * FROM pickup_rider_details WHERE RIDER_NUMBER = '"+phoneNumber+"' AND Password = '"+passkey+"'" ;
                //PreparedStatement stmt = conor.prepareStatement(st1);
                //stmt.setString(1,phoneNumber);
                //stmt.setString(2,passkey);
                ResultSet setting = stmt.executeQuery(st1);

                if (setting.next()) {
                    counter = -100;

                    //assignedRiderId = set.getString("HUB_RIDER_ID");
                    //set.beforeFirst();
                }

                String st2 = "SELECT * FROM unauthorized_riders WHERE NUMBER ='"+phoneNumber+"' AND Password = '"+passkey+"' ";
                ResultSet set1 = stmt.executeQuery(st2);

                //String st2 = "SELECT * FROM unauthorized_riders WHERE NUMBER =? AND Password =? ";
                //PreparedStatement stmt2 = conor.prepareStatement(st2);
                //stmt2.setString(1,phoneNumber);
                //stmt2.setString(2,passkey);
                //ResultSet set1 = stmt.executeQuery();
                if(set1.next()){
                    counter =-200;

                }

                //conor.close();

            }

            catch (Exception e) {

                e.printStackTrace();
            }


            return null ;
        }

        @Override
        protected void onPostExecute(Object o) {
            p.dismiss();
            if(counter==-100){
                Toast.makeText(Login.this, "Login Successful", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Login.this, MainActivity.class);
                intent.putExtra("Phone", editText.getText().toString());
                intent.putExtra("Password", editText2.getText().toString());
                // intent.putExtra("Pickupboy_ID",assignedRiderId);

                startActivity(intent);

            } else {

                Toast.makeText(Login.this, "Invalid Details", Toast.LENGTH_SHORT).show();
            }
            if(counter==-200){
                Toast.makeText(Login.this, "Visit NearBy Hub For Verification", Toast.LENGTH_SHORT).show();
            }

            }


        }



    private String[] checkForPasswordFile() {
        File login= new File(Environment.getExternalStorageDirectory()+"/Android/data/ShipDotsSailer/login.txt");
        String[] det_array = null;
        if(login.exists()) {
            String line;
            String details = "";
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(login)));
                while ((line = reader.readLine())!=null) {
                    details = line;

                }
                det_array = details.split(" ");

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return det_array;
    }

    public void vibrate() {
        Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(100);
        }
    }


}
